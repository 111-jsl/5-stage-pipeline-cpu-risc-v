`ifndef __PIPES_SV
`define __PIPES_SV


`ifdef VERILATOR
`include "include/common.sv"
`else

`endif

package pipes;
	import common::*;

	/* Define instrucion decoding rules here */
	//F7
	parameter F7_ALUI = 7'b0010011; 
	parameter F7_ALU = 7'b0110011;
	parameter F7_LUI = 7'b0110111;
	parameter F7_ALUIW = 7'b0011011;
	parameter F7_ALUW = 7'b0111011;

	parameter F7_JAL = 7'b1101111;
	
	parameter F7_BR = 7'b1100011; //branch: beq, bne...
	parameter F7_AUIPC = 7'b0010111;
	parameter F7_JALR = 7'b1100111;

	//load & save
	parameter F7_LD = 7'b0000011;
	parameter F7_SD = 7'b0100011;
	
	parameter F7_CSR = 7'b1110011;
	parameter F7_NOP = 7'b0000000;


	//F3
	//alui
	parameter F3_ADDI = 3'b000;
	parameter F3_XORI = 3'b100;
	parameter F3_ORI = 3'b110;
	parameter F3_ANDI = 3'b111;
	parameter F3_SLTI = 3'b010;
	parameter F3_SLTIU = 3'b011;
	parameter F3_SLI = 3'b001;
	parameter F3_SRI = 3'b101;

	//aluiw
	parameter F3_ADDIW = 3'b000;
	parameter F3_SLIW = 3'b001;
	parameter F3_SRIW = 3'b101;

	//aluw
	parameter F3_ASMW = 3'b000; // addw & subw & mulw
	parameter F3_SLW = 3'b001;
	parameter F3_SRW_DIVUW = 3'b101;
	parameter F3_DIVW = 3'b100;
	parameter F3_REMW = 3'b110;
	parameter F3_REMUW = 3'b111;

	//br
	parameter F3_BEQ = 3'b000;
	parameter F3_BNE = 3'b001;
	parameter F3_BLT = 3'b100;
	parameter F3_BGE = 3'b101;
	parameter F3_BLTU = 3'b110;
	parameter F3_BGEU = 3'b111;

	//load & save
	//load
	parameter F3_LD = 3'b011;
	parameter F3_LB = 3'b000;
	parameter F3_LH = 3'b001;
	parameter F3_LW = 3'b010;
	parameter F3_LBU = 3'b100;
	parameter F3_LHU = 3'b101;
	parameter F3_LWU = 3'b110;
	//save
	parameter F3_SD = 3'b011;
	parameter F3_SB = 3'b000;
	parameter F3_SH = 3'b001;
	parameter F3_SW = 3'b010;


	//alu
	parameter F3_ASM = 3'b000; // add & sub & mul
	parameter F3_AND_REMU = 3'b111; // and & remu
	parameter F3_OR_REM = 3'b110; // or & rem
	parameter F3_XOR_DIV = 3'b100; // xor & div
	parameter F3_SL = 3'b001;
	parameter F3_SR_DIVU = 3'b101; // sr & divu
	parameter F3_SLT = 3'b010;
	parameter F3_SLTU = 3'b011;

	parameter F3_JALR = 3'b000;

	parameter F3_RC = 3'b011;
	parameter F3_RCI = 3'b111;
	parameter F3_RS = 3'b010;
	parameter F3_RSI = 3'b110;
	parameter F3_RW = 3'b001;
	parameter F3_RWI = 3'b101;
	parameter F3_SPEC = 3'b000;

	parameter F3_NOP = 3'b000;



	//ff7, f6
	//alu
	parameter FF7_ADD = 7'b0000000;
	parameter FF7_SUB = 7'b0100000;
	parameter FF7_AND = 7'b0000000;
	parameter FF7_OR = 7'b0000000;
	parameter FF7_REM = 7'b0000001;
	parameter FF7_REMU = 7'b0000001;
	parameter FF7_XOR = 7'b0000000;
	parameter FF7_MUL = 7'b0000001;
	parameter FF7_DIV = 7'b0000001;
	parameter FF7_DIVU = 7'b0000001;

	//alui
	parameter F6_SLLI = 6'b000000;
	parameter F6_SRLI = 6'b000000;
	// parameter F6_SLAI = 6'b010000;
	parameter F6_SRAI = 6'b010000;
	parameter FF7_SLL = 7'b0000000;
	parameter FF7_SRL = 7'b0000000;
	parameter FF7_SRA = 7'b0100000;
	parameter FF7_SLT = 7'b0000000;
	parameter FF7_SLTU = 7'b0000000;

	//aluiw
	parameter F6_SLLIW = 6'b000000;
	parameter F6_SRLIW = 6'b000000;
	parameter F6_SRAIW = 6'b010000;

	//aluw
	parameter FF7_ADDW = 7'b0000000;
	parameter FF7_SUBW = 7'b0100000;
	parameter FF7_SLLW = 7'b0000000;
	parameter FF7_SRLW = 7'b0000000;
	parameter FF7_SRAW = 7'b0100000;
	parameter FF7_MULW = 7'b0000001;
	parameter FF7_DIVW = 7'b0000001;
	parameter FF7_DIVUW = 7'b0000001;
	parameter FF7_REMW = 7'b0000001;
	parameter FF7_REMUW = 7'b0000001;
	parameter FF7_ECALL = 7'b0000000;
	parameter FF7_MRET = 7'b0011000;

	parameter FF7_NOP = 7'b0000000;

	

	/* Define pipeline structures here */

	typedef u7 f7_t;
	typedef u3 f3_t;
	typedef u6 f6_t;
	//common
	// typedef i64 addr_t;
	// typedef i64 word_t;


	typedef enum logic [6:0] {
		ALU_ADD,
		ALU_XOR,
		ALU_OR,
		ALU_AND,
		ALU_SUB,

		ALU_MUL,
		//mul div -- signed
		ALU_DIV,
		ALU_REM,
		//mul div -- unsigned
		ALU_DIVU,
		ALU_REMU,
		//shift-64
		ALU_SLL,
		ALU_SRL,
		ALU_SRA,
		//shift-32
		ALU_SLLW,
		ALU_SRLW,
		ALU_SRAW,
		//slt
		ALU_SLT,
		ALU_SLTU
	} alufunc_t;

	typedef enum logic [7:0] { 
		NOP,
		//alui
		OP_ADDI,
		OP_XORI,
		OP_ORI,
		OP_ANDI,
		OP_SLTI,
		OP_SLTIU,
		OP_SLLI,
		OP_SRLI,
		OP_SRAI,

		//aluiw
		OP_ADDIW,
		OP_SLLIW,
		OP_SRLIW,
		OP_SRAIW,

		//aluw
		OP_ADDW,
		OP_SUBW,
		OP_SLLW,
		OP_SRLW,
		OP_SRAW,
		OP_MULW,
		OP_DIVW,
		OP_DIVUW,
		OP_REMW,
		OP_REMUW,

		//alu
		OP_ADD,
		OP_SUB,
		OP_XOR,
		OP_OR,
		OP_AND,
		OP_SLL,
		OP_SRL,
		OP_SRA,
		OP_SLT,
		OP_SLTU,
		OP_MUL,
		OP_DIV,
		OP_DIVU,
		OP_REM,
		OP_REMU,

		OP_LUI,

		OP_JAL,

		//br
		OP_BEQ,
		OP_BNE,
		OP_BLT,
		OP_BGE,
		OP_BLTU,
		OP_BGEU,

		//load & save
		//load
		OP_LD,
		OP_LB,
		OP_LH,
		OP_LW,
		OP_LBU,
		OP_LHU,
		OP_LWU,
		//save
		OP_SD,
		OP_SB,
		OP_SH,
		OP_SW,
		

		OP_AUIPC,
		OP_JALR,

		OP_CSRRC,
		OP_CSRRCI,
		OP_CSRRS,
		OP_CSRRSI,
		OP_CSRRW,
		OP_CSRRWI,
		OP_MRET,
		OP_ECALL
	} op_t;

	parameter IMM_SRC = 2'b00;
	parameter REG_SRC = 2'b01;
	parameter CSR_SRC = 2'b10;
	typedef struct packed {
		alufunc_t alufunc;
		u2 alusrc;
		op_t op;
		u1 regwrite;
		u1 mem_write;
		u1 memtoreg;
		u1 branch;
		u1 en_ra1, en_ra2;
		u1 result_sext;
		u1 csrwrite;
		// u1 exception_trigger;
		u1 is_csr;
		// msize_t msize;
	} control_t;

	typedef struct packed {
		u1 exception_trigger;
		u4 mcause;
		u64 mtval;
	} priv_inst_t;

	typedef struct packed {
		word_t srca, srcb;
		creg_addr_t ra1, ra2;
		word_t mwdata;
		control_t ctl;
		creg_addr_t dst;
		u64 imm;
		u64 pc;
		csr_addr_t csr_ra;
		u64 csr_rd;
		priv_inst_t priv_inst;
	} decode_data_t;

	typedef struct packed {
		u64 pc;
		u64 result;
		word_t mwdata;
		control_t ctl;
		creg_addr_t dst;
		csr_addr_t csr_wa;
		u64 csr_rd;
		priv_inst_t priv_inst;
	} execute_data_t;

	typedef struct packed {
		u32 raw_instr;
		u64 pc;
		priv_inst_t priv_inst;
	} fetch_data_t;

	typedef struct packed {
		u64 pc;
		u64 result;
		word_t md;
		control_t ctl;
		creg_addr_t dst;
		csr_addr_t csr_wa;
		u64 csr_rd;
		priv_inst_t priv_inst;
	} memory_data_t;


	typedef struct packed {
		// u64 pc;
		// control_t ctl;
		// u64 result;
		u64 pc;
		u64 wd;
		creg_addr_t wa;
		csr_addr_t csr_wa;
		u64 csr_wd;
		control_t ctl;
		priv_inst_t priv_inst;
	} writeback_data_t;

endpackage
`endif
