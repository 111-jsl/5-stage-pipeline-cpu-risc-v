`ifndef __DCACHE_SV
`define __DCACHE_SV

`ifdef VERILATOR
`include "include/common.sv"
/* You should not add any additional includes in this file */
`endif













module DCache 
	import common::*; #(
		/* You can modify this part to support more parameters */
		/* e.g. OFFSET_BITS, INDEX_BITS, TAG_BITS */
		parameter X = 1,
        parameter WORDS_PER_LINE = 16,
        parameter ASSOCIATIVITY = 2,
        parameter SET_NUM = 8,

        parameter OFFSET_BITS = $clog2(WORDS_PER_LINE),
        parameter INDEX_BITS = $clog2(SET_NUM),
        // parameter SET_BITS = $clog2(ASSOCIATIVITY),
        parameter TAG_BITS = 32 - INDEX_BITS - OFFSET_BITS - 3, /* Maybe 32, or smaller */
        parameter META_VOID_BITS = 32 - TAG_BITS - 2,
        parameter RESET_TIME = 127

        
	)(
	input logic clk, reset,

	input  dbus_req_t  dreq,
    output dbus_resp_t dresp,
    output cbus_req_t  creq,
    input  cbus_resp_t cresp
);

`ifndef REFERENCE_CACHE

	/* TODO: Lab3 Cache */
    //typedef
    
    localparam type state_t = enum logic[2:0] {
            INIT, 
            FETCH, 
            WRITEHIT,
            WRITEBACK,
            SKIP,
            RESET,
            READY
        };
    localparam type offset_t = logic [OFFSET_BITS-1:0];
    localparam type index_t = logic [INDEX_BITS-1:0];
    localparam type tag_t = logic [TAG_BITS-1:0];
    localparam type meta_void_t = logic [META_VOID_BITS-1:0];
    typedef struct packed {
        u1 valid;
        u1 dirty;
        tag_t tag;
        meta_void_t meta_void; // add to 32bits
    } meta_t;

    typedef struct packed {
        logic    en;
        strobe_t strobe;
        
        word_t wdata;
    } ram_data_t;

    typedef u8 age_t;

    typedef struct packed {
        logic    en;
        u1 strobe;
       
        meta_t wmeta;
    } ram_meta_t;

    //function
    function offset_t get_offset(addr_t addr);
        return addr[3+OFFSET_BITS-1:3];
    endfunction

    function index_t get_index(addr_t addr);
        return addr[3+INDEX_BITS+OFFSET_BITS-1:OFFSET_BITS+3];
    endfunction

    function tag_t get_tag(addr_t addr);
        return addr[3+INDEX_BITS+OFFSET_BITS+TAG_BITS-1:3+INDEX_BITS+OFFSET_BITS];
    endfunction
    
    //ram
    ram_data_t ram_data[ASSOCIATIVITY-1:0];
    u64 rdata[ASSOCIATIVITY-1:0];
    ram_meta_t ram_meta[ASSOCIATIVITY-1:0];
    meta_t rmeta[ASSOCIATIVITY-1:0];
    offset_t offset;
    // offset_t mem_offset;
    index_t meta_ram_addr;
    u7 data_ram_addr;

    //reset
    u7 reset_counter;

    //bus
    word_t resp_data;
    addr_t creq_addr;
    word_t creq_data;
    // dbus_req_t req;

    //lru
    age_t age[SET_NUM-1:0][ASSOCIATIVITY-1:0];
    age_t counter;
    u1[ASSOCIATIVITY-1:0] _lru_replace;
    u1[ASSOCIATIVITY-1:0] lru_replace;
    u1 write_back;

    //fsm
    state_t state;

    //signal
    u1[ASSOCIATIVITY-1:0] hit;
    u1 read_ready, write_ready;

    //skip
    u1 skip;
    
    //ref-model debug
    // strobe_t ref_strobe;
    // assign ref_strobe = dreq.strobe;


    //initializing ram
    // initial begin
    //     for(int i = 0; i < SET_NUM; i++) begin
    //         age[i][0] = '0; age[i][1] = '0;
    //     end
    //     for(int i = 0; i < ASSOCIATIVITY; i++) begin
    //         ram_data[i] = '0; rdata[i] = '0;
    //         ram_meta[i] = '0; rmeta[i] = '0;
    //     end
    //     offset = '0; 
    //     resp_data = '0;
    //     creq_addr = '0; creq_data = '0;
    //     counter = 2;
    //     _lru_replace = '0; lru_replace = '0;
    //     write_back = '0;
    //     state = INIT;
    //     hit = '0; read_ready = '0; write_ready = '0;
    //     skip = '0;
    // end

    assign read_ready = (state == INIT) && (|hit) && ~(|dreq.strobe);
    assign write_ready = (state == WRITEHIT);

    //counter increment
    always_ff@(posedge clk) begin
        if(reset || counter[7]) 
            counter <= 2;
        else
            counter <= counter + 1;
        // else begin

        // end
    end

    //calculate age
    for(genvar i = 0; i < SET_NUM; i++) begin: counter_overflow
        always_ff @(posedge clk) begin
            if(counter[7]) begin
                if(age[i][0] > age[i][1]) begin
                    age[i][0] <= 1;
                    age[i][1] <= 0;
                end else begin
                    age[i][0] <= 0;
                    age[i][1] <= 1;
                end
                if(i == get_index(dreq.addr)) begin
                    if(hit[0]) begin
                        age[i][0] <= 1;
                        age[i][1] <= 0;
                    end
                    if(hit[1]) begin
                        age[i][0] <= 0;
                        age[i][1] <= 1;
                    end
                end
            end else begin
                if(state == INIT && i == get_index(dreq.addr)) begin
                    if(hit[0]) age[i][0] <= counter;
                    if(hit[1]) age[i][1] <= counter;
                end
            end
        end
    end: counter_overflow

    // always_ff @(posedge clk) begin
    //     if(~counter[7] && state == INIT) begin
            
            
           
    //     end 

    // end
    offset_t ram_offset;
    assign ram_offset = state == INIT || state == WRITEHIT ? get_offset(dreq.addr) : offset;

    
    

    //cache instances
    RAM_SinglePort #(
        .ADDR_WIDTH(INDEX_BITS + OFFSET_BITS),
        .DATA_WIDTH($bits(word_t) * ASSOCIATIVITY),
        .BYTE_WIDTH(8),
        .MEM_TYPE(0),
        .READ_LATENCY(0)
    ) data_ram (
        .clk,  .en('1),
        .addr(data_ram_addr),
        .strobe({ram_data[1].strobe, ram_data[0].strobe}),
        .wdata({ram_data[1].wdata, ram_data[0].wdata}),
        .rdata({rdata[1], rdata[0]})
    );
    RAM_SinglePort #(
        .ADDR_WIDTH(INDEX_BITS),
        .DATA_WIDTH($bits(meta_t) * ASSOCIATIVITY),
        .BYTE_WIDTH($bits(meta_t)),
        .MEM_TYPE(0),
        .READ_LATENCY(0)
    ) meta_ram (
        .clk,  .en('1),
        .addr(meta_ram_addr),
        .strobe({ram_meta[1].strobe, ram_meta[0].strobe}),
        .wdata({ram_meta[1].wmeta, ram_meta[0].wmeta}),
        .rdata({rmeta[1], rmeta[0]})
    );
    

    always_comb begin
        if(state == RESET) begin
            meta_ram_addr = reset_counter[6:4];
            data_ram_addr = reset_counter;
        end else begin
            meta_ram_addr = get_index(dreq.addr);
            data_ram_addr = {get_index(dreq.addr), ram_offset};
        end
    end


    
    //lru
    always_comb begin
        _lru_replace = '0;
        write_back = '0;
        if(state == INIT) begin
            unique case({rmeta[0].valid, rmeta[1].valid})
                2'b00: _lru_replace[0] = 1'b1;
                2'b01: _lru_replace[0] = 1'b1;
                2'b10: _lru_replace[1] = 1'b1;
                2'b11: begin
               
                    if (age[get_index(dreq.addr)][0] > age[get_index(dreq.addr)][1]) begin
                        _lru_replace = 2'b10;
                        write_back = rmeta[1].dirty;
                    end else begin
                        _lru_replace = 2'b01;
                        write_back = rmeta[0].dirty;
                    end
                
                end
                default: begin

                end
            endcase
        end
    end

    //skip
    assign skip = dreq.addr[31] == 1'b0;
    
    //hit
    for(genvar i = 0; i < ASSOCIATIVITY; i++) begin: hit_behavior
        always_comb begin
            hit[i] = '0;
            if(rmeta[i].valid && get_tag(dreq.addr) == rmeta[i].tag) begin
                //hit signal
                hit[i] = dreq.valid;
            
                
            end
        end
        
    end: hit_behavior

    always_comb begin
        resp_data = '0;
        unique case(hit)
            2'b10: resp_data = rdata[1];
            2'b01: resp_data = rdata[0];
            default: begin

            end
        endcase
    end
    

    // //lru_select
    // for(genvar i = 0; i < ASSOCIATIVITY; i++) begin: lru_replacing
    //     always_comb begin
    //         if(state == FETCH && lru_replace[i]) begin
    //             ram_data[i].strobe = '1;
    //             ram_data[i].wdata = cresp.data;
    //             ram_meta[i].strobe = '1;
    //             ram_meta[i].wmeta.valid = 1'b1;
    //             ram_meta[i].wmeta.dirty = 1'b0;
    //             ram_meta[i].wmeta.tag = get_tag(dreq.addr);
                
    //         end
    //     end
    // end: lru_replacing
   
    //fsm behavior
    always_comb begin
        // offset = get_offset(dreq.addr);
        ram_data[0] = '0; ram_data[1] = '0;
        ram_meta[0] = '0; ram_meta[1] = '0;
        creq_addr = '0;
        creq_data = '0;
        unique case(state)
            INIT: begin
                // ram_data[0].en = dreq.valid & ~skip;
                // ram_meta[0].en = dreq.valid & ~skip;
                ram_data[0].strobe = '0; 
                ram_data[1].strobe = '0;
                ram_meta[0].strobe = '0; 
                ram_meta[1].strobe = '0;
                
            end 
            WRITEHIT: begin
                //data
                ram_data[0].strobe = {8{hit[0]}} & dreq.strobe;
                ram_data[1].strobe = {8{hit[1]}} & dreq.strobe;
                ram_data[0].wdata = dreq.data;
                ram_data[1].wdata = dreq.data;
                //meta
                ram_meta[0].strobe = hit[0];
                ram_meta[1].strobe = hit[1];
                ram_meta[0].wmeta = rmeta[0];
                ram_meta[0].wmeta.dirty = 1'b1;
                ram_meta[1].wmeta = rmeta[1];
                ram_meta[1].wmeta.dirty = 1'b1;
                
            end
            FETCH: begin
                creq_addr = {dreq.addr[63:7], 7'b0000000};
                // offset = mem_offset;
                if(lru_replace[0]) begin
                    ram_data[0].strobe = '1;
                    ram_data[0].wdata = cresp.data;
                    ram_meta[0].strobe = '1;
                    ram_meta[0].wmeta.valid = 1'b1;
                    ram_meta[0].wmeta.dirty = 1'b0;
                    ram_meta[0].wmeta.tag = get_tag(dreq.addr);
                end else begin
                    ram_data[1].strobe = '1;
                    ram_data[1].wdata = cresp.data;
                    ram_meta[1].strobe = '1;
                    ram_meta[1].wmeta.valid = 1'b1;
                    ram_meta[1].wmeta.dirty = 1'b0;
                    ram_meta[1].wmeta.tag = get_tag(dreq.addr);
                end
            end
            WRITEBACK: begin
                //ram_data
                // ram_data[0].en = '1;
                // ram_meta[0].en = '1;
                ram_data[0].strobe = '0; 
                ram_data[1].strobe = '0;
                ram_meta[0].strobe = '0; 
                ram_meta[1].strobe = '0;
                // offset = mem_offset;
               
                unique case(lru_replace)
                    2'b10: begin
                        creq_addr = {dreq.addr[63:32], rmeta[1].tag, get_index(dreq.addr), 7'b0000000};
                        creq_data = rdata[1];
                        // $display(creq_addr);
                    end
                    2'b01: begin
                        creq_addr = {dreq.addr[63:32], rmeta[0].tag, get_index(dreq.addr), 7'b0000000};
                        creq_data = rdata[0];
                        // $display(creq_addr);
                    end
                    default: begin

                    end
                endcase
            end
            SKIP: begin
                creq_addr = dreq.addr;
                creq_data = dreq.data;
               
            end
            RESET: begin
                ram_meta[0].strobe = '1; ram_meta[1].strobe = '1;
                ram_meta[0].wmeta = '0; ram_meta[1].wmeta = '0;
                ram_data[0].strobe = '1; ram_data[1].strobe = '1;
                ram_data[0].wdata = '0; ram_data[1].wdata = '0;
            end 
            default: begin

            end
        endcase
    end

    // DBus driver
    assign dresp.addr_ok = state == INIT;
    assign dresp.data_ok = state == SKIP ? cresp.last : read_ready || write_ready;
    assign dresp.data    = state == SKIP ? cresp.data : resp_data;

    // CBus driver
    assign creq.valid    = state == FETCH || state == WRITEBACK || state == SKIP;
    assign creq.is_write = state == SKIP ? |dreq.strobe : state == WRITEBACK;
    assign creq.size     = state == SKIP ? dreq.size : MSIZE8;
    assign creq.addr     = creq_addr;
    assign creq.strobe   = state == SKIP ? dreq.strobe : '1;
    assign creq.data     = creq_data;
    assign creq.len      = state == SKIP ? MLEN1 : MLEN16;
	assign creq.burst	 = state == SKIP ? AXI_BURST_FIXED : AXI_BURST_INCR;


    //reset
    always_ff @(posedge clk) begin
        if(state == RESET) begin
            reset_counter <= reset_counter + 1;
        end else if(reset_counter == RESET_TIME) begin

        end else begin
            reset_counter <= '0;
        end
    end
    


    //fsm
    always_ff@(posedge clk) begin
        if(~reset) begin
            unique case(state)
                INIT: begin
                    // mem_offset <= '0;
                    if(dreq.valid) begin
                        // offset <= get_offset(dreq.addr);
                        if (|hit) begin
                            // if(hit[0]) age[get_index(dreq.addr)][0] <= counter;
                            // else age[get_index(dreq.addr)][1] <= counter;
                            state <= (|dreq.strobe) ? WRITEHIT : INIT;
                        end else begin
                            state <= write_back ? WRITEBACK : FETCH;
                            // mem_offset <= '0;
                            offset <= '0;
                            lru_replace <= _lru_replace;
                        end
                        if (skip) begin
                            state <= SKIP;
                        end
                    end
                end
                READY: state <= INIT;
                WRITEHIT: begin
                    // mem_offset <= '0;
                    state <= INIT;
                end
                FETCH: begin
                    // if(~counter[7]) begin
                    //     if(lru_replace[0]) 
                    //         age[get_index(dreq.addr)][0] <= counter;
                    //     else 
                    //         age[get_index(dreq.addr)][1] <= counter;
                    // end
                    if (cresp.ready) begin
                        state  <= cresp.last ? INIT : FETCH;
                        // mem_offset <= mem_offset + 1;
                        offset <= offset + 1;
                        
                    end
                end
                WRITEBACK: begin
                    if (cresp.ready) begin
                        state <= cresp.last ? FETCH : WRITEBACK;
                        // mem_offset <= mem_offset + 1;
                        offset <= offset + 1;
                    end
                end
                SKIP: begin
                    // mem_offset <= '0;
                    if (cresp.last) begin
                        state <= INIT;
                    end else begin
                        state <= SKIP;
                    end
                end
                RESET: begin
                    // mem_offset <= '0;
                    state <= INIT;
                end
                default: begin
                    // mem_offset <= '0;

                end
                
            endcase
        end else begin
            if(reset_counter == RESET_TIME) begin
                // reset_counter <= '0;
                state <= INIT;
            end else begin
                state <= RESET;
            end
            offset <= '0;
            // mem_offset <= '0;
            lru_replace <= '0;
        end
    end

`else

	typedef enum u2 {
		IDLE,
		FETCH,
		READY,
		FLUSH
	} state_t /* verilator public */;

	// typedefs
    typedef union packed {
        word_t data;
        u8 [7:0] lanes;
    } view_t;

    typedef u4 offset_t;

    // registers
    state_t    state /* verilator public_flat_rd */;
    dbus_req_t req;  // dreq is saved once addr_ok is asserted.
    offset_t   offset;

    // wires
    offset_t start;
    assign start = dreq.addr[6:3];

    // the RAM
    struct packed {
        logic    en;
        strobe_t strobe;
        word_t   wdata;
    } ram;
    word_t ram_rdata;

    always_comb
    unique case (state)
    FETCH: begin
        ram.en     = 1;
        ram.strobe = 8'b11111111;
        ram.wdata  = cresp.data;
    end

    READY: begin
        ram.en     = 1;
        ram.strobe = req.strobe;
        ram.wdata  = req.data;
    end

    default: ram = '0;
    endcase

    RAM_SinglePort #(
		.ADDR_WIDTH(4),
		.DATA_WIDTH(64),
		.BYTE_WIDTH(8),
		.READ_LATENCY(0)
	) ram_inst (
        .clk(clk), .en(ram.en),
        .addr(offset),
        .strobe(ram.strobe),
        .wdata(ram.wdata),
        .rdata(ram_rdata)
    );

    // DBus driver
    assign dresp.addr_ok = state == IDLE;
    assign dresp.data_ok = state == READY;
    assign dresp.data    = ram_rdata;

    // CBus driver
    assign creq.valid    = state == FETCH || state == FLUSH;
    assign creq.is_write = state == FLUSH;
    assign creq.size     = MSIZE8;
    assign creq.addr     = req.addr;
    assign creq.strobe   = 8'b11111111;
    assign creq.data     = ram_rdata;
    assign creq.len      = MLEN16;
	assign creq.burst	 = AXI_BURST_INCR;

    // the FSM
    always_ff @(posedge clk)
    if (~reset) begin
        unique case (state)
        IDLE: if (dreq.valid) begin
            state  <= FETCH;
            req    <= dreq;
            offset <= start;
        end

        FETCH: if (cresp.ready) begin
            state  <= cresp.last ? READY : FETCH;
            offset <= offset + 1;
        end

        READY: begin
            state  <= (|req.strobe) ? FLUSH : IDLE;
        end

        FLUSH: if (cresp.ready) begin
            state  <= cresp.last ? IDLE : FLUSH;
            offset <= offset + 1;
        end

        endcase
    end else begin
        state <= IDLE;
        {req, offset} <= '0;
    end

`endif

endmodule

`endif
