`ifndef __ALU_SV
`define __ALU_SV
`ifdef VERILATOR
`include "include/common.sv"
`include "include/pipes.sv"
`include "pipeline/execute/divider.sv"
`else

`endif


module alu
	import common::*;
	import pipes::*; (
	input logic clk, reset,
	output logic stall_alu,

	input u64 a, b,
	input alufunc_t alufunc,
	output u64 c
);
	u1 done;
	u64 _c, _r;
	u64 _a, _b;
	u1 div_en;

	// initial begin
	// 	_c = '0; _r = '0;
	// end
	always_comb begin
		_a = a; _b = b;
		if(alufunc == ALU_DIV || alufunc == ALU_REM) begin
			if(a[63]) _a = ~a + 1;
			if(b[63]) _b = ~b + 1;
		end
		
	end

	always_comb begin
		c = '0;
		stall_alu = 1'b0;
		div_en = 1'b0;
		
		
		unique case(alufunc)
			ALU_ADD: c = a + b;
			ALU_SUB: c = a - b;
			ALU_AND: c = a & b;
			ALU_OR: c = a | b;
			ALU_XOR: c = a ^ b;
			
			ALU_MUL: begin
				c = _a * _b;
 			end
			//div --signed
			ALU_DIV: begin
				if(b == 0) c = '1;
				else begin
					div_en = 1'b1;
					if(_c == '0) c = '0;
					else if(a[63] ^ b[63]) begin
						c = ~_c + 1;
					end else begin
						c = _c;
					end
					stall_alu = ~done;
				end
                
			end
			
			//div --unsigned
			ALU_DIVU: begin
				if(b == 0) c = '1;
				else begin
					div_en = 1'b1;
					c = _c;
					stall_alu = ~done;
				end

			end
			ALU_REM: begin
				if(b == 0) c = a;
				else begin
					div_en = 1'b1;
					if(_r == '0) c = '0;
					else if(a[63]) begin
						c = ~_r + 1;
					end else begin
						c = _r;
					end
					stall_alu = ~done;
				end
				
			end
			ALU_REMU: begin
				if(b == 0) c = a;
				else begin
					div_en = 1'b1;
					c = _r;
					stall_alu = ~done;
				end
			end
			//shift-64
			ALU_SLL: c = a << b[5:0];
			ALU_SRL: c = a >> b[5:0];
			ALU_SRA: c = $signed(a) >>> b[5:0];
			//shift-32
			ALU_SLLW: c = {{32{1'b0}}, a[31:0] << b[4:0]};
			ALU_SRLW: c = {{32{1'b0}}, a[31:0] >> b[4:0]};
			ALU_SRAW: c = {{32{1'b0}}, $signed(a[31:0]) >>> b[4:0]};
			//slt
			ALU_SLT: begin
				if($signed(a) < $signed(b)) c = 1;
				else c = 0;
			end
			ALU_SLTU: begin
				if(a < b) c = 1;
				else c = 0;
			end
			default: begin
				
			end
		endcase
		// $display("alu data:");
		// $display(a,b,c);

	end

	
	divider divider(
		.clk,
		.reset,
		.valid(div_en),
		.a(_a),
		.b(_b),
		.done,
		.c(_c),
		.r(_r)
	);
	
endmodule

`endif
