// `ifndef __MULTIPLIER_SV
// `define __MULTIPLIER_SV
// `ifdef VERILATOR
// `include "include/common.sv"
// `include "include/pipes.sv"
// `else

// `endif

// module multiplier
//     import common::*;
//     import pipes::*;
//     (
//         input logic clk, resetn, valid,
//         input i64 a, b,
//         output logic done,
//         output i64 c // c = a * b
//     );

//     // c = (a[31:16] * b[31:16] << 32) + (a[15:0] * b[31:16] << 16) +
//     //     (a[31:16] * b[15:0] << 32) + (a[15:0] * b[15:0])

//     logic [15:0][31:0]p, p_nxt;
//     assign p_nxt[0] = a[15:0] * b[15:0];
//     assign p_nxt[1] = a[15:0] * b[31:16];
//     assign p_nxt[2] = a[15:0] * b[47:32];
//     assign p_nxt[3] = a[15:0] * b[63:48];
//     assign p_nxt[4] = a[31:16] * b[15:0];
//     assign p_nxt[5] = a[31:16] * b[31:16];
//     assign p_nxt[6] = a[31:16] * b[47:32];
//     assign p_nxt[7] = a[31:16] * b[63:48];
//     assign p_nxt[8] = a[47:32] * b[15:0];
//     assign p_nxt[9] = a[47:32] * b[31:16];
//     assign p_nxt[10] = a[47:32] * b[47:32];
//     assign p_nxt[11] = a[47:32] * b[63:48];
//     assign p_nxt[12] = a[63:48] * b[15:0];
//     assign p_nxt[13] = a[63:48] * b[31:16];
//     assign p_nxt[14] = a[63:48] * b[47:32];
//     assign p_nxt[15] = a[63:48] * b[63:48];
    

//     always_ff @(posedge clk) begin
//         if (~resetn) begin
//             p <= '0;
//         end else begin
//             p <= p_nxt;
//         end
//     end
//     always_comb begin

//     end
//     logic [3:0][63:0] q;
//     assign q[0] = {p[0]};
//     assign q[1] = {p[1], 16'b0};
//     assign q[2] = {p[2], 16'b0};
//     assign q[3] = {p[3], 32'b0};
//     assign c = q[0] + q[1] + q[2] + q[3];

//     enum logic {INIT, DOING} state, state_nxt;
//     always_ff @(posedge clk) begin
//         if (~resetn) begin
//             state <= INIT;
//         end else begin
//             state <= state_nxt;
//         end
//     end
//     always_comb begin
//         state_nxt = state;
//         if (state == DOING) begin
//             state_nxt = INIT;
//         end else if (valid) begin
//             state_nxt = DOING;
//         end
//     end
//     assign done = state_nxt == INIT;

// endmodule
// `endif