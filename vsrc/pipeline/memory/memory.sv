`ifndef __MEMORY_SV
`define __MEMORY_SV
`ifdef VERILATOR
`include "include/common.sv"
`include "include/pipes.sv"
`include "pipeline/memory/readdata.sv"
`include "pipeline/memory/writedata.sv"
`else

`endif


module memory 
    import common::*;
    import pipes::*;
    import csr_pkg::*;
    (
        input logic clk, reset,
        input logic stalling, flush,

        input execute_data_t dataE,
        output memory_data_t dataM,
        input word_t md_beforecheck,
        output u64 ma,
        output word_t wd_aftercheck,
        output strobe_t strobe_aftercheck,
        output logic valid,
        input logic dataW_nxt_is_csr
    );

    //pipeline
    memory_data_t dataM_nxt;
    always_ff @(posedge clk) begin
        if(reset) dataM <= '0;
        else if(stalling) begin

        end
        else if(flush) dataM <= '0;
        else dataM <= dataM_nxt;
    end

    word_t md_aftercheck;
    assign dataM_nxt.pc = dataE.pc;
    assign dataM_nxt.md = md_aftercheck;
    assign dataM_nxt.dst = dataE.dst;

    assign dataM_nxt.result = dataE.result;
    assign dataM_nxt.csr_wa = dataE.csr_wa;
    assign dataM_nxt.csr_rd = dataE.csr_rd;

    priv_inst_t priv_inst;
    assign dataM_nxt.ctl = dataE.ctl;

    msize_t msize;
    u1 mem_unsigned;
    assign dataM_nxt.priv_inst = priv_inst;

    

    
    always_comb begin
        msize = MSIZE8;
        mem_unsigned = 1'b0;
        priv_inst = dataE.priv_inst;
        unique case(dataE.ctl.op)
            OP_LB: begin
                msize = MSIZE1;
                mem_unsigned = 1'b0;
            end
            OP_LH: begin
                msize = MSIZE2;
                if(dataE.result[0]) begin
                    priv_inst.mcause = LOAD_ADDR;
                    priv_inst.exception_trigger = 1'b1;
                    priv_inst.mtval = dataE.result;
                end
                mem_unsigned = 1'b0;
            end
            OP_LW: begin
                msize = MSIZE4;
                if(|dataE.result[1:0]) begin
                    priv_inst.mcause = LOAD_ADDR;
                    priv_inst.exception_trigger = 1'b1;
                    priv_inst.mtval = dataE.result;
                end
                mem_unsigned = 1'b0;
            end
            OP_LD: begin
                msize = MSIZE8;
                if(|dataE.result[2:0]) begin
                    priv_inst.mcause = LOAD_ADDR;
                    priv_inst.exception_trigger = 1'b1;
                    priv_inst.mtval = dataE.result;
                end
                mem_unsigned = 'x;
            end
            OP_LBU: begin
                msize = MSIZE1;
                mem_unsigned = 1'b1;
            end
            OP_LHU: begin
                msize = MSIZE2;
                if(dataE.result[0]) begin
                    priv_inst.mcause = LOAD_ADDR;
                    priv_inst.exception_trigger = 1'b1;
                    priv_inst.mtval = dataE.result;
                end
                
                mem_unsigned = 1'b1;
            end
            OP_LWU: begin
                msize = MSIZE4;
                if(|dataE.result[1:0]) begin
                    priv_inst.mcause = LOAD_ADDR;
                    priv_inst.exception_trigger = 1'b1;
                    priv_inst.mtval = dataE.result;
                end
                mem_unsigned = 1'b1;
            end
            OP_SB: begin
                
                msize = MSIZE1;
            end
            OP_SH: begin
                if(dataE.result[0]) begin
                    priv_inst.mcause = STORE_ADDR;
                    priv_inst.exception_trigger = 1'b1;
                    priv_inst.mtval = dataE.result;
                end
                msize = MSIZE2;
            end 
            OP_SW: begin
                if(|dataE.result[1:0]) begin
                    priv_inst.mcause = STORE_ADDR;
                    priv_inst.exception_trigger = 1'b1;
                    priv_inst.mtval = dataE.result;
                end
                msize = MSIZE4; 
            end
            OP_SD: begin
                if(|dataE.result[2:0]) begin
                    priv_inst.mcause = STORE_ADDR;
                    priv_inst.exception_trigger = 1'b1;
                    priv_inst.mtval = dataE.result;
                end
                msize = MSIZE8;
            end
            default: begin

            end
        endcase
    end
    
    // always_comb begin
    //     $display("dataM:");
    //     $display(dataM);
    // end
    
    readdata readdata(
        .md_beforecheck,
        .md_aftercheck,
        .addr(dataE.result[2:0]),
        .msize,
        .mem_unsigned
    );

    strobe_t strobe_beforecheck;
    writedata writedata(
        .wd_beforecheck(dataE.mwdata),
        .wd_aftercheck,
        .addr(dataE.result[2:0]),
        .msize,
        .strobe_beforecheck
    );
    // always_comb begin
    //     // if(dataE.ctl.memtoreg) begin
    //     //     $display(dataE.pc);
    //     //     $display(dataE.result);

    //     // end
    //     if(|dataE.pc)
    //         $display(dataE.pc);
    // end
    // always_ff @(posedge clk) begin
    //     if(|strobe_aftercheck) begin
    //         // $display("data writed in cache");
    //         $display(wd_aftercheck);
    //     end
    // end
    assign ma = dataE.result;
    assign valid = (dataE.ctl.memtoreg || dataE.ctl.mem_write) && ~(priv_inst.exception_trigger || dataW_nxt_is_csr);
    assign strobe_aftercheck = strobe_beforecheck & {8{dataE.ctl.mem_write}};
endmodule
`endif    