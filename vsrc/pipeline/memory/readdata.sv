`ifndef __READDATA_SV
`define __READDATA_SV
`ifdef VERILATOR
`include "include/common.sv"
`include "include/pipes.sv"
`else

`endif

module readdata
    import common::*;
    import pipes::*;
    (
        input u64 md_beforecheck,
        output u64 md_aftercheck,
        input u3 addr,
        input msize_t msize,
        input u1 mem_unsigned
    );
    u1 sign_bit;
    always_comb begin
        md_aftercheck = 'x;
        sign_bit = 'x;
        unique case(msize)
            MSIZE1: begin
                unique case(addr)
                    3'b000: begin
                        sign_bit = mem_unsigned? 1'b0: md_beforecheck[7];
                        md_aftercheck = {{56{sign_bit}}, md_beforecheck[7-:8]};
                    end
                    3'b001: begin
                        sign_bit = mem_unsigned? 1'b0: md_beforecheck[15];
                        md_aftercheck = {{56{sign_bit}}, md_beforecheck[15-:8]};
                    end
                    3'b010: begin
                        sign_bit = mem_unsigned? 1'b0: md_beforecheck[23];
                        md_aftercheck = {{56{sign_bit}}, md_beforecheck[23-:8]};
                    end
                    3'b011: begin
                        sign_bit = mem_unsigned? 1'b0: md_beforecheck[31];
                        md_aftercheck = {{56{sign_bit}}, md_beforecheck[31-:8]};
                    end
                    3'b100: begin
                        sign_bit = mem_unsigned? 1'b0: md_beforecheck[39];
                        md_aftercheck = {{56{sign_bit}}, md_beforecheck[39-:8]};
                    end
                    3'b101: begin
                        sign_bit = mem_unsigned? 1'b0: md_beforecheck[47];
                        md_aftercheck = {{56{sign_bit}}, md_beforecheck[47-:8]};
                    end
                    3'b110: begin
                        sign_bit = mem_unsigned? 1'b0: md_beforecheck[55];
                        md_aftercheck = {{56{sign_bit}}, md_beforecheck[55-:8]};
                    end
                    3'b111: begin
                        sign_bit = mem_unsigned? 1'b0: md_beforecheck[63];
                        md_aftercheck = {{56{sign_bit}}, md_beforecheck[63-:8]};
                    end
                    default: begin
                        
                    end
                endcase
            end
            MSIZE2: begin
                unique case(addr)
                    3'b000: begin
                        sign_bit = mem_unsigned? 1'b0: md_beforecheck[15];
                        md_aftercheck = {{48{sign_bit}}, md_beforecheck[15-:16]};
                    end
                    3'b010: begin
                        sign_bit = mem_unsigned? 1'b0: md_beforecheck[31];
                        md_aftercheck = {{48{sign_bit}}, md_beforecheck[31-:16]};
                    end
                    3'b100: begin
                        sign_bit = mem_unsigned? 1'b0: md_beforecheck[47];
                        md_aftercheck = {{48{sign_bit}}, md_beforecheck[47-:16]};
                    end
                    3'b110: begin
                        sign_bit = mem_unsigned? 1'b0: md_beforecheck[63];
                        md_aftercheck = {{48{sign_bit}}, md_beforecheck[63-:16]};
                    end
                    default: begin
                        
                    end
                endcase
            end
            MSIZE4: begin
                unique case(addr)
                    3'b000: begin
                        sign_bit = mem_unsigned? 1'b0: md_beforecheck[31];
                        md_aftercheck = {{32{sign_bit}}, md_beforecheck[31-:32]};
                    end
                    3'b100: begin
                         sign_bit = mem_unsigned? 1'b0: md_beforecheck[63];
                        md_aftercheck = {{32{sign_bit}}, md_beforecheck[63-:32]};
                    end
                    default: begin
                        
                    end
                endcase
            end
            MSIZE8: begin
                unique case(addr)
                    3'b000: begin
                        md_aftercheck = md_beforecheck;
                    end
                    default: begin
                        
                    end
                endcase
            end
            default: begin
                
            end
        endcase
    end




endmodule
`endif

