// `ifndef __STALL_SV
// `define __STALL_SV

// `ifdef VERILATOR
// `include "include/common.sv"
// `include "include/pipes.sv"
// `else
 
// `endif


// module stall 
//     import common::*;
//     import pipes::*;
//     (
//         input decode_data_t dataD,
//         input execute_data_t dataE,
//         input memory_data_t dataM,
//         output u1 stalling
//     );

//     u1 c1;
//     assign c1 = dataE.ctl.regwrite && dataE.dst != '0 &&
//                 (dataE.dst == dataD.ra1 || dataE.dst == dataD.ra2);

//     u1 c2;
//     assign c2 = dataM.ctl.regwrite && dataM.dst != '0 &&
//                 (dataM.dst == dataD.ra1 || dataM.dst == dataD.ra2);

//     assign stalling = c1 || c2;


// endmodule
// `endif