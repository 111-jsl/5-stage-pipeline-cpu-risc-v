`ifndef __BUBBLE_SV
`define __BUBBLE_SV

`ifdef VERILATOR
`include "include/common.sv"
`include "include/pipes.sv"
`else
 
`endif


module bubble
    import common::*;
    import pipes::*;
    (
        input u1 branch_taken,
        output u1 jump
    );

    assign jump = branch_taken;

endmodule
`endif