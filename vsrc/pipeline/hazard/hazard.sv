`ifndef __HAZARD_SV
`define __HAZARD_SV

`ifdef VERILATOR
`include "include/common.sv"
`include "include/pipes.sv"
`include "pipeline/hazard/bubble.sv"
// `include "pipeline/hazard/stall.sv"
`include "pipeline/hazard/forward.sv"
`else
 
`endif


module hazard
    import common::*;
    import pipes::*;
    (
        input u1 branch_taken,
        output u1 jump,
        input decode_data_t dataD,
        input execute_data_t dataE,
        input memory_data_t dataM,
        input writeback_data_t dataW,
        output decode_data_t dataD_forward,
        output u1 stall_hazard,
        output creg_addr_t ra1, ra2,
        input u64 rd1, rd2,
        output csr_addr_t csr_ra,
        input u64 csr_rd
    );

    bubble bubble(
        .branch_taken,
        .jump
    );

    // stall stall(
    //     .dataD,
    //     .dataE,
    //     .dataM,
    //     .stalling
    // );
    
    forward forward(
        .dataD,
        .dataE,
        .dataM,
        .dataW,
        .dataD_forward,
        .stall_hazard,
        .ra1,
        .ra2,
        .rd1,
        .rd2,
        .csr_ra,
        .csr_rd
    );

    


endmodule
`endif