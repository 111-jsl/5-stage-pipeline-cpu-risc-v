`ifndef __FORWARD_SV
`define __FORWARD_SV

`ifdef VERILATOR
`include "include/common.sv"
`include "include/pipes.sv"
`else
 
`endif

//转发与阻塞
//alu->reg or mem->reg 在e和m阶段直接转发
//mem->reg 在e阶段需要阻塞前面的流水线
//可能会有e阶段阻塞、m阶段转发，此时d阶段收不到转发值
//那么m阶段再次转发， 之后直接存入reg，无需转发

module forward
    import common::*;
    import pipes::*;
    (
        input decode_data_t dataD,
        input execute_data_t dataE,
        input memory_data_t dataM,
        input writeback_data_t dataW,
        output decode_data_t dataD_forward,
        output u1 stall_hazard,
        output creg_addr_t ra1, ra2,
        input u64 rd1, rd2,
        output csr_addr_t csr_ra,
        input u64 csr_rd

    );

    u1 ce1;
    assign ce1 = dataE.ctl.regwrite && dataE.dst != '0 &&
                dataE.dst == dataD.ra1 && dataD.ctl.en_ra1;

    u1 ce2;
    assign ce2 = dataE.ctl.regwrite && dataE.dst != '0 &&
                dataE.dst == dataD.ra2 && dataD.ctl.en_ra2;

    u1 cm1;
    assign cm1 = dataM.ctl.regwrite && dataM.dst != '0 &&
                dataM.dst == dataD.ra1 && dataD.ctl.en_ra1 && ~ce1;

    u1 cm2;
    assign cm2 = dataM.ctl.regwrite && dataM.dst != '0 &&
                dataM.dst == dataD.ra2 && dataD.ctl.en_ra2 && ~ce2;

    u1 cw1;
    assign cw1 = dataW.ctl.regwrite && dataW.wa != '0 &&
                dataW.wa == dataD.ra1 && dataD.ctl.en_ra1 && ~ce1 && ~cm1;

    u1 cw2;
    assign cw2 = dataW.ctl.regwrite && dataW.wa != '0 &&
                dataW.wa == dataD.ra2 && dataD.ctl.en_ra2 && ~ce2 && ~cm2;

    assign ra1 = dataD.ra1;
    assign ra2 = dataD.ra2;
    assign csr_ra = dataD.csr_ra;

    assign stall_hazard = (ce1 || ce2) && dataE.ctl.memtoreg;

    assign dataD_forward.ra1 = dataD.ra1;
    assign dataD_forward.ra2 = dataD.ra2;
    assign dataD_forward.ctl = dataD.ctl;
    assign dataD_forward.dst = dataD.dst;
    assign dataD_forward.imm = dataD.imm;
    assign dataD_forward.pc = dataD.pc;
    assign dataD_forward.csr_rd = csr_rd;
    assign dataD_forward.csr_ra = dataD.csr_ra;
    assign dataD_forward.priv_inst = dataD.priv_inst;

    always_comb begin
        if(dataD.ctl.en_ra1) dataD_forward.srca = rd1;
        else dataD_forward.srca = dataD.imm;

        if(ce1 && ~dataE.ctl.memtoreg) begin
            
            dataD_forward.srca = dataE.result;
            
        end else if(cm1) begin
            if(dataM.ctl.memtoreg == 1'b1)
                dataD_forward.srca = dataM.md;
            else
                dataD_forward.srca = dataM.result;
        end else if(cw1) begin
            dataD_forward.srca = dataW.wd;
        end else begin

        end
    end
    
    always_comb begin
        unique case(dataD.ctl.alusrc)
            IMM_SRC: dataD_forward.srcb = dataD.srcb;
            REG_SRC: dataD_forward.srcb = rd2;
            CSR_SRC: dataD_forward.srcb = csr_rd;
            default: dataD_forward.srcb = dataD.srcb;
        endcase
        
        dataD_forward.mwdata = rd2;

        

        if(ce2 && ~dataE.ctl.memtoreg) begin
            
            if(dataD.ctl.alusrc == REG_SRC) 
                dataD_forward.srcb = dataE.result;
            
            dataD_forward.mwdata = dataE.result;
            
        end if(cm2) begin
            if(dataM.ctl.memtoreg == 1'b1) begin
                if(dataD.ctl.alusrc == REG_SRC) 
                    dataD_forward.srcb = dataM.md;
                dataD_forward.mwdata = dataM.md;
            end else begin
                if(dataD.ctl.alusrc == REG_SRC) 
                    dataD_forward.srcb = dataM.result;
                dataD_forward.mwdata = dataM.result;
            end
        end else if(cw2) begin
            if(dataD.ctl.alusrc == REG_SRC) 
                dataD_forward.srcb = dataW.wd;
            dataD_forward.mwdata = dataW.wd;
        end else begin

        end
        
    end

endmodule
`endif