`ifndef __DECODE_SV
`define __DECODE_SV

`ifdef VERILATOR
`include "include/common.sv"
`include "include/pipes.sv"
`include "pipeline/decode/decoder.sv"
`include "pipeline/decode/sext.sv"
`else
 
`endif


module decode
    import common::*;
    import pipes::*;
    import csr_pkg::*;
    (
        input logic clk, reset,
        input logic stalling,
        input logic flush,


        input fetch_data_t dataF,
       
        output decode_data_t dataD
        // input word_t rd1, rd2,
        // output creg_addr_t ra1, ra2,
        // output word_t imm
        
    );
    control_t ctl;
    word_t imm;
    priv_inst_t priv_inst;
    decoder decoder(
        .raw_instr(dataF.raw_instr),
        .priv_inst_nxt(dataF.priv_inst),
        .priv_inst,
        .ctl(ctl)
    );
    sext sext(
        .raw_instr(dataF.raw_instr),
        .op(ctl.op),
        .imm
    );


    //pipeline
    decode_data_t dataD_nxt_beforecheck;
    decode_data_t dataD_nxt_aftercheck;
    always_ff @(posedge clk) begin
        if(reset) dataD <= '0;
        else if(stalling) begin

        end
        else if(flush) dataD <= '0;
        else dataD <= dataD_nxt_aftercheck;
    end

    //debug
    // always_comb begin
    //     $display("dataD:");
    //     $display(dataD);
    // end
    assign dataD_nxt_beforecheck.dst = dataF.raw_instr[11:7];
    // assign ra2 = dataF.raw_instr[24:20];
    // assign ra1 = dataF.raw_instr[19:15];

    

    assign dataD_nxt_beforecheck.pc = dataF.pc;
    assign dataD_nxt_beforecheck.mwdata = '0;
    assign dataD_nxt_beforecheck.ctl = ctl;
    assign dataD_nxt_beforecheck.ra1 = dataF.raw_instr[19:15];
    assign dataD_nxt_beforecheck.ra2 = dataF.raw_instr[24:20];
    assign dataD_nxt_beforecheck.imm = imm;
    assign dataD_nxt_beforecheck.srca = '0;
    assign dataD_nxt_beforecheck.srcb = imm;
    assign dataD_nxt_beforecheck.csr_ra = dataF.raw_instr[31:20];
    assign dataD_nxt_beforecheck.priv_inst = priv_inst;
    // always_comb begin
    //     dataD_nxt_beforecheck.srca = rd1;
    //     if(ctl.alusrc) dataD_nxt_beforecheck.srcb = rd2;
    //     else dataD_nxt_beforecheck.srcb = imm;
    // end

    always_comb begin
        dataD_nxt_aftercheck = dataD_nxt_beforecheck;
        unique case(dataD_nxt_beforecheck.ctl.op)
            OP_SRAIW: begin
                if(dataD_nxt_beforecheck.imm[5] == 0) begin

                end else begin
                    dataD_nxt_aftercheck = '0;
                end
            end
            OP_SLLIW: begin
                if(dataD_nxt_beforecheck.imm[5] == 0) begin

                end else begin
                    dataD_nxt_aftercheck = '0;
                end
            end
            OP_SRLIW: begin
                if(dataD_nxt_beforecheck.imm[5] == 0) begin

                end else begin
                    dataD_nxt_aftercheck = '0;
                end
            end
            

            default: begin

            end
        endcase
    end


    
endmodule
`endif