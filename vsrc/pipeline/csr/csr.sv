`ifndef __CSR_SV
`define __CSR_SV


`ifdef VERILATOR
`include "include/csr_pkg.sv"

`else

`endif

module csr
	import common::*;
	import pipes::*;
	import csr_pkg::*;(
	input logic clk, reset,
	input logic valid,
	input logic is_mret,
	input csr_addr_t ra, wa,
	input u64 wd,
	input u64 pc, pcs_pc, dataF_pc, dataD_pc, dataE_pc,
	output u64 rd,
	output u64 mepc,
	output u64 mtvec,
	input priv_inst_t priv_inst,
	input u1 trint, swint, exint,
	output u1 interrupt_trigger,
	output csr_regs_t commit_regs_nxt,
	output u2 commit_mode_nxt

	// csr_intf.csr self,
	// pcselect_intf.csr pcselect

);
	csr_regs_t regs, regs_nxt;
	u2 mode, mode_nxt;
	// logic swint, trint, exint;
	initial begin
		regs = '0;
		regs_nxt = '0;
	end
	assign commit_regs_nxt = regs_nxt;
	assign commit_mode_nxt = mode_nxt;
	always_ff @(posedge clk) begin
		if (reset) begin
			regs <= '0;
			regs.mcause[1] <= 1'b1;
			regs.mepc[31] <= 1'b1;
			mode <= MACHINE;
		end else begin
			regs <= regs_nxt;
			mode <= mode_nxt;
		end
	end

	// read
	always_comb begin
		rd = '0;
		unique case(ra)
			CSR_MIE: rd = regs.mie;
			CSR_MIP: rd = regs.mip;
			CSR_MTVEC: rd = regs.mtvec;
			CSR_MSTATUS: rd = regs.mstatus;
			CSR_MSCRATCH: rd = regs.mscratch;
			CSR_MEPC: rd = regs.mepc;
			CSR_MCAUSE: rd = regs.mcause;
			CSR_MCYCLE: rd = regs.mcycle;
			CSR_MTVAL: rd = regs.mtval;
			default: begin
				rd = '0;
			end
		endcase
	end

	// write
	always_comb begin
		regs_nxt = regs;
		mode_nxt = mode;
		regs_nxt.mip[M_SWINT_IDX] = swint && regs.mie[M_SWINT_IDX] && regs.mstatus.mie;
		regs_nxt.mip[M_TRINT_IDX] = trint && regs.mie[M_TRINT_IDX] && regs.mstatus.mie;
		regs_nxt.mip[M_EXINT_IDX] = exint && regs.mie[M_EXINT_IDX] && regs.mstatus.mie;
		
		// Writeback: W stage
		unique if (valid) begin
			unique case(wa)
				CSR_MIE: regs_nxt.mie = wd;
				CSR_MIP:  regs_nxt.mip = wd;
				CSR_MTVEC: regs_nxt.mtvec = wd;
				CSR_MSTATUS: regs_nxt.mstatus = wd;
				CSR_MSCRATCH: regs_nxt.mscratch = wd;
				CSR_MEPC: regs_nxt.mepc = wd;
				CSR_MCAUSE: regs_nxt.mcause = wd;
				CSR_MCYCLE: regs_nxt.mcycle = wd;
				CSR_MTVAL: regs_nxt.mtval = wd;
				default: begin
					
				end
				
			endcase
			regs_nxt.mstatus.sd = regs_nxt.mstatus.fs != 0;
		end else if (is_mret) begin
			regs_nxt.mstatus.mie = regs_nxt.mstatus.mpie;
			regs_nxt.mstatus.mpie = 1'b1;
			regs_nxt.mstatus.mpp = 2'b0;
			regs_nxt.mip = '0;
			regs_nxt.mstatus.xs = 0;
			mode_nxt = USER;
		end else begin end
		if(priv_inst.exception_trigger || 
			(((swint && regs.mie[M_SWINT_IDX]) || 
			(trint && regs.mie[M_TRINT_IDX]) || 
			(exint && regs.mie[M_EXINT_IDX])) && regs.mstatus.mie)) begin

			if(pc != 0) regs_nxt.mepc = pc;
			else if(dataE_pc != 0) regs_nxt.mepc = dataE_pc;
			else if(dataD_pc != 0) regs_nxt.mepc = dataD_pc;
			else if(dataF_pc != 0) regs_nxt.mepc = dataF_pc;
			else if(pcs_pc != 0) regs_nxt.mepc = pcs_pc;
			else begin end

			if(priv_inst.exception_trigger) begin
				if(priv_inst.mcause == U_ECALL && mode == MACHINE) regs_nxt.mcause[62:0] = {{59{1'b0}}, M_ECALL};
				else regs_nxt.mcause[62:0] = {{59{1'b0}}, priv_inst.mcause};
			end
			else if(swint && regs.mie[M_SWINT_IDX]) begin
				regs_nxt.mcause[62:0] = {{59{1'b0}}, M_SWINT};
				
			end
			else if(trint && regs.mie[M_TRINT_IDX]) begin
				regs_nxt.mcause[62:0] = {{59{1'b0}}, M_TRINT};
				
			end
			else begin
				regs_nxt.mcause[62:0] = {{59{1'b0}}, M_EXINT};
			end
			
			regs_nxt.mcause[63] = ~(priv_inst.exception_trigger);
			regs_nxt.mstatus.mpie = regs_nxt.mstatus.mie;
			regs_nxt.mstatus.mie = '0;
			regs_nxt.mstatus.mpp = mode_nxt;
			regs_nxt.mtval = priv_inst.mtval;
			mode_nxt = MACHINE;

		end
	end
	assign mepc = regs.mepc;
	
	assign mtvec = regs.mtvec;
	assign interrupt_trigger = ((swint && regs.mie[M_SWINT_IDX]) || 
							   (trint && regs.mie[M_TRINT_IDX]) || 
							   (exint && regs.mie[M_EXINT_IDX])) && regs.mstatus.mie;
endmodule

`endif