`ifndef __PCSELECT_SV
`define __PCSELECT_SV

`ifdef VERILATOR
`include "include/common.sv"
`include "include/pipes.sv"
`else
 
`endif

module pcselect
    import common::*;
    import pipes::*;
    (
        input logic clk, reset,
        input logic stalling,
        input logic stall_hazard,
        input u1 branch_taken,
        input u64 branch_target,
        input u64 pc_plus4,
        output u64 pc,
        //save branch_target
        input u1 data_ok,
        output logic illegal_pc
    );


    u64 pc_nxt;
    u64 branch_target_save;
    always_comb begin
        pc_nxt = pc_plus4;
        if(branch_taken)
            pc_nxt = branch_target;
        
    end
    // always_comb begin
    //     if(pc >= 64'h8000_0000) begin
    //         $display("signal of pc:");
    //         $display(stalling);
    //     end
    // end
    always_ff @(posedge clk) begin
        if(branch_taken && ~data_ok && ~stall_hazard) branch_target_save <= branch_target;
       
		if(reset) pc <= 64'h8000_0000;
        else if(stalling) begin
            
        end else if(|branch_target_save) begin
            pc <= branch_target_save;
            branch_target_save <= '0;
        end else pc <= pc_nxt;
	end
    
    assign illegal_pc = pc[0] == 1'b1 || pc[1] == 1'b1;

endmodule
`endif