`ifndef __FETCH_SV
`define __FETCH_SV

`ifdef VERILATOR
`include "include/common.sv"
`include "include/pipes.sv"
`include "include/csr_pkg.sv"
`else
 
`endif

//instruction register
module fetch
    import common::*;
    import pipes::*;
    import csr_pkg::*;
    (
        input logic clk, reset,
        input logic flush, 
        input logic stalling,
        input logic stall_hazard,

        input u32 raw_instr,
        input u64 pc,
        output fetch_data_t dataF,

        //avoid flush signal loss
        input u1 branch_taken, data_ok
        
    );

    //pipeline
    fetch_data_t dataF_nxt;
    u1 flush_save;
    always_ff @(posedge clk) begin
        //save flush signal
        if(branch_taken && ~data_ok && ~stall_hazard) flush_save <= 1'b1;
        // if(flush_ibus && ~branch_taken) flush_save <= '0;
        if(reset) dataF <= '0;
        else if(stalling) begin

        end else if(flush) begin
            dataF <= '0;
        end else if(flush_save) begin
            dataF <= '0;
            flush_save <= 1'b0;
        end else 
            dataF <= dataF_nxt;

    end

    
    // single-cycle cpu
    assign dataF_nxt.raw_instr = raw_instr;
    assign dataF_nxt.pc = pc;
    always_comb begin
        dataF_nxt.priv_inst = '0;
        if(pc[0] == 1'b1 || pc[1] == 1'b1) begin
            dataF_nxt.priv_inst.mcause = INST_ADDR;
            // dataF_nxt.priv_inst.mtval = pc;
            dataF_nxt.priv_inst.exception_trigger = 1'b1;
        end
    end
    //debug
    // always_comb begin
    //     $display("dataF: ");
    //     $display(dataF.pc);
    //     $display(dataF.raw_instr);
    // end

endmodule
`endif