`include "access.svh"
`ifdef VERILATOR
`include "include/common.sv"
/* You should not add any additional includes in this file */
`endif


module VCacheTop 
    import common::*;
    (
    input logic clk, reset_,

    input  dbus_req_t  dreq,
    output dbus_resp_t dresp,
    output cbus_req_t  creq,
    input  cbus_resp_t cresp
);
    `include "bus_decl"

    // cbus_req_t  dcreq;
    // cbus_resp_t dcresp;

    // assign creq = dcreq;
    // assign dcresp = cresp;
	wire reset = reset_;
    DCache top(.*);

    
    // word_t mask;
`ifdef REFERENCE_CACHE
	word_t  mem [15:0]/* verilator public_flat_rd */;
	for (genvar i = 0; i < 16; i++)
    	assign mem[i] = top.ram_inst.mem[i];
`else
    /**
     * TODO (Lab3, optional) expose internal memory to simulator
     *
     * NOTE: it will slow down FST tracing significantly, especially
     *       if your cache is large, you may want to speed up by adding
     *       "// verilator tracing_off" before the declaration of
     *       the variable mem.
     */

    /**
     * the following is an example. Suppose that you used LUTRAM and
     * your cache contains only four cache lines, each of which consists of
     * 16 consecutive words in memory.
     *
     * later you can access the variable mem from C++ via VCacheTop->mem.
     * it will possibly become a 1d array of uint32_t.
     */
    // typedef word_t cache_line_t;
    
    /* verilator tracing_off */
    word_t mem[7'h7f:0][1:0] /* verilator public_flat_rd */;
    // u1 lru[1:0];
    // u7 mem_addr[15:0];
    /* verilator tracing_on */
    // u4 icounter;
    // assign lru[0] = lru_replace[0];
    // assign lru[1] = lru_replace[1];
    // assign mem_addr[0] = {dreq.addr[9:7], 4'h0};
    // assign mem_addr[1] = {dreq.addr[9:7], 4'h1};
    // assign mem_addr[2] = {dreq.addr[9:7], 4'h2};
    // assign mem_addr[3] = {dreq.addr[9:7], 4'h3};
    // assign mem_addr[4] = {dreq.addr[9:7], 4'h4};
    // assign mem_addr[5] = {dreq.addr[9:7], 4'h5};
    // assign mem_addr[6] = {dreq.addr[9:7], 4'h6};
    // assign mem_addr[7] = {dreq.addr[9:7], 4'h7};
    // assign mem_addr[8] = {dreq.addr[9:7], 4'h8};
    // assign mem_addr[9] = {dreq.addr[9:7], 4'h9};
    // assign mem_addr[10] = {dreq.addr[9:7], 4'ha};
    // assign mem_addr[11] = {dreq.addr[9:7], 4'hb};
    // assign mem_addr[12] = {dreq.addr[9:7], 4'hc};
    // assign mem_addr[13] = {dreq.addr[9:7], 4'hd};
    // assign mem_addr[14] = {dreq.addr[9:7], 4'he};
    // assign mem_addr[15] = {dreq.addr[9:7], 4'hf};

    // assign mask = 
    // {
    //     {8{dreq.strobe[7]}}, 
    //     {8{dreq.strobe[6]}},
    //     {8{dreq.strobe[5]}},
    //     {8{dreq.strobe[4]}},
    //     {8{dreq.strobe[3]}},
    //     {8{dreq.strobe[2]}},
    //     {8{dreq.strobe[1]}},
    //     {8{dreq.strobe[0]}}
    // };


    for (genvar i = 0; i < 128; i++) begin
       
        assign mem[i][0] = top.data_ram.mem[i][63:0];
        assign mem[i][1] = top.data_ram.mem[i][127:64];
    
    end
    // for (genvar i = 0; i < 16; i++) begin
        
    //     always_comb begin
    //         unique case(top.hit) 
    //             2'b01: begin
    //                 mem[i] = top.data_ram.mem[mem_addr[i]][63:0];
    //             end
    //             2'b10: begin
    //                 mem[i] = top.data_ram.mem[mem_addr[i]][127:64];
    //             end
    //             default: begin

    //             end
    //         endcase
    //     end
    // end
   
`endif
endmodule
