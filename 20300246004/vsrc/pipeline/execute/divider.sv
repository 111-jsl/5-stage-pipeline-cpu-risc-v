`ifndef __DIVIDER_SV
`define __DIVIDER_SV
`ifdef VERILATOR
`include "include/common.sv"
`include "include/pipes.sv"
`else

`endif

module divider
    import common::*;
    import pipes::*;
    (
        input logic clk, reset, valid,
        input i64 a, b,
        output logic done,
        output i64 c, // c = {a % b, a / b}
        output i64 r
    );


    enum i1 { INIT, DOING } state, state_nxt;
    i33 count, count_nxt;
    localparam i33 DIV_DELAY = {1'b1, 32'b0};
    always_ff @(posedge clk) begin
        if (reset) begin
            {state, count} <= '0;
        end else begin
            {state, count} <= {state_nxt, count_nxt};
        end
    end
    always_comb begin
        {state_nxt, count_nxt} = {state, count}; // default
        unique case(state)
            INIT: begin
                if (valid) begin
                    
                    state_nxt = DOING;
                    count_nxt = DIV_DELAY;
                end
            end
            DOING: begin
                count_nxt = {1'b0, count_nxt[32:1]};
                if (count_nxt == '0) begin
                    state_nxt = INIT;
                end
            end
            default: begin

            end
        endcase
    end

    // u1 done_nxt;
    assign done = (state_nxt == INIT);
    // always_ff @(posedge clk) begin
    //     if(reset) begin
    //         done <= 1'b0;
    //     end else begin
    //         done <= done_nxt;
    //         $display(done_nxt);
    //     end 
    // end



    i128 p, p_nxt;

    always_ff @(posedge clk) begin
        if (reset) begin
            p <= '0;
        end else begin
            p <= p_nxt;
        end
    end
    always_comb begin
        p_nxt = p;
        unique case(state)
            INIT: begin
                
                p_nxt = {{64{1'b0}}, a};
            end
            DOING: begin
                p_nxt = {p_nxt[126:0], 1'b0};
                if (p_nxt[127:64] >= b) begin
                    p_nxt[127:64] -= b;
                    p_nxt[0] = 1'b1;
                end
                p_nxt = {p_nxt[126:0], 1'b0};
                if (p_nxt[127:64] >= b) begin
                    p_nxt[127:64] -= b;
                    p_nxt[0] = 1'b1;
                end
            end
            default: begin

            end
        endcase
    end
    


    assign c = p[63:0];
    assign r = p[127:64];

    // always_comb begin
    //     if(valid) begin
    //         $display(a);
    //         $display(b);
    //         $display(c);
    //         $display(r);
    //     end
    // end

endmodule
`endif