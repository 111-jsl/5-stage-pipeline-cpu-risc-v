`ifndef __EXECUTE_SV
`define __EXECUTE_SV
`ifdef VERILATOR
`include "include/common.sv"
`include "include/pipes.sv"
`include "pipeline/execute/alu.sv"
`else

`endif

module execute
    import common::*;
    import pipes::*;
    (
        input logic clk, reset,
        input logic flush, stalling,

        input decode_data_t dataD,
        output execute_data_t dataE,

        output u1 _branch_taken,
        output u64 _branch_target,
        output u1 stall_alu
    );

    word_t result_beforecheck, result_aftercheck;
    word_t a, b;
    // u1 csign, rsign;
    alu alu(
        .clk,
        .reset,
        .stall_alu,
        .a,
        .b,
        .alufunc(dataD.ctl.alufunc),
        .c(result_beforecheck)
    );

    // always_comb begin
    //     $display("dataE:");
    //     $display(dataE);
    // end

    //special values
    always_comb begin
        a = dataD.srca;
        b = dataD.srcb;
        unique case(dataD.ctl.op) 
            OP_LUI: begin
                a = '0;
            end
            OP_JAL: begin
                a = dataD.pc;
                b = 4;
            end
            OP_JALR: begin
                a = dataD.pc;
                b = 4;
            end
            OP_AUIPC: begin
                a = dataD.pc;
            end
            OP_DIVW: begin
                a = {{32{dataD.srca[31]}}, dataD.srca[31:0]};
                b = {{32{dataD.srcb[31]}}, dataD.srcb[31:0]};
            end
            OP_DIVUW: begin
                a = {{32{1'b0}}, dataD.srca[31:0]};
                b = {{32{1'b0}}, dataD.srcb[31:0]};
            end
            OP_REMW: begin
                a = {{32{dataD.srca[31]}}, dataD.srca[31:0]};
                b = {{32{dataD.srcb[31]}}, dataD.srcb[31:0]};
            end
            OP_REMUW: begin
                a = {{32{1'b0}}, dataD.srca[31:0]};
                b = {{32{1'b0}}, dataD.srcb[31:0]};
            end
            OP_CSRRC: a = ~dataD.srcb;
            OP_CSRRCI: a = ~dataD.srcb;
            OP_CSRRW: b = '0;
            OP_CSRRWI: b = '0;
            default: begin

            end
        endcase
    end

    //branch
    always_comb begin
        if(stalling) begin
            _branch_target = '0;
        end else begin
            _branch_target = dataD.pc;
            unique case(dataD.ctl.op) 
                OP_JAL: _branch_target = dataD.pc + dataD.imm;
                OP_JALR: _branch_target = (dataD.srca + dataD.imm);

                OP_BEQ: _branch_target = dataD.pc + dataD.imm;
                OP_BNE: _branch_target = dataD.pc + dataD.imm;
                OP_BLT: _branch_target = dataD.pc + dataD.imm;
                OP_BGE: _branch_target = dataD.pc + dataD.imm;
                OP_BLTU: _branch_target = dataD.pc + dataD.imm;
                OP_BGEU: _branch_target = dataD.pc + dataD.imm;
                default: begin

                end
            endcase
        end
    end

    always_comb begin
        if(stalling) begin
            _branch_taken = '0;
        end else begin
            _branch_taken = dataD.ctl.branch;
            unique case(dataD.ctl.op)
                OP_BEQ: begin
                    if(dataD.srca == dataD.srcb) _branch_taken = 1'b1;
                end
                OP_BNE: begin
                    if(dataD.srca != dataD.srcb) _branch_taken = 1'b1;
                end
                OP_BLT: begin
                    if($signed(dataD.srca) < $signed(dataD.srcb)) _branch_taken = 1'b1;
                end
                OP_BGE: begin
                    if($signed(dataD.srca) >= $signed(dataD.srcb)) _branch_taken = 1'b1;
                end
                OP_BLTU: begin
                    if(dataD.srca < dataD.srcb) _branch_taken = 1'b1;
                end
                OP_BGEU: begin
                    if(dataD.srca >= dataD.srcb) _branch_taken = 1'b1;
                end
                default: begin

                end
            endcase
        end
    end


    //check
    always_comb begin
        result_aftercheck = result_beforecheck;
        if(dataD.ctl.result_sext) begin
            result_aftercheck = {
                {32{result_beforecheck[31]}},
                result_beforecheck[31:0]
            };
        end
    end


    //pipeline
    execute_data_t dataE_nxt;
    always_ff @(posedge clk) begin
        if(reset) dataE <= '0;
        else if(stalling) begin
            
        end
        else if(flush) dataE <= '0;
        else begin
            dataE <= dataE_nxt;
        end
    end

    // //debug
    // always_comb begin
    //     if(dataD.ctl.op == OP_SD) begin
    //         $display(dataD.pc);
    //         $display(dataD.srca);
    //         $display(dataD.srcb);
    //         $display(result_beforecheck);
    //     end
    // end


    //single-cycle cpu
    assign dataE_nxt.pc = dataD.pc;
    assign dataE_nxt.result = result_aftercheck;
    assign dataE_nxt.dst = dataD.dst;
    assign dataE_nxt.ctl = dataD.ctl;
    assign dataE_nxt.mwdata = dataD.mwdata;
    assign dataE_nxt.csr_wa = dataD.csr_ra;
    assign dataE_nxt.csr_rd = dataD.csr_rd;
    assign dataE_nxt.priv_inst = dataD.priv_inst;

endmodule
`endif

