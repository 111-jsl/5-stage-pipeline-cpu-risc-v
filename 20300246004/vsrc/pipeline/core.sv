`ifndef __CORE_SV
`define __CORE_SV
`ifdef VERILATOR
`include "include/common.sv"
// `include "include/pipes.sv"
`include "pipeline/regfile/regfile.sv"
`include "pipeline/fetch/fetch.sv"
`include "pipeline/fetch/pcselect.sv"
`include "pipeline/decode/decode.sv"
`include "pipeline/execute/execute.sv"
`include "pipeline/memory/memory.sv"
`include "pipeline/writeback/writeback.sv"
`include "pipeline/hazard/hazard.sv"
`include "pipeline/csr/csr.sv"
`else

`endif

module core 
	import common::*;
	import pipes::*;
	import csr_pkg::*;
	(
	input logic clk, reset,
	input logic trint, swint, exint,
	output ibus_req_t  ireq,
	input  ibus_resp_t iresp,
	output dbus_req_t  dreq,
	input  dbus_resp_t dresp
);
	/* TODO: Add your pipeline here. */

	// u1 commit_valid;
	// u64 commit_pc_save;

	//pipeline datapack
	u64 pc, mepc, mtvec;
	u1 branch_taken, _branch_taken;
	u64 branch_target, _branch_target;

	u1 jump;
	decode_data_t dataD_forward;

	u32 raw_instr;
	fetch_data_t dataF;
	
	decode_data_t dataD;
	word_t rd1, rd2;
	creg_addr_t ra1, ra2;
	word_t imm;

	execute_data_t dataE;

	memory_data_t dataM;

	u1 stall_hazard, flush_hazard;
	u1 stall_dbus, flush_dbus;
	u1 stall_ibus, flush_ibus;
	u1 stall_alu, flush_alu;

	writeback_data_t dataW, dataW_nxt;

	logic illegal_daddr, illegal_inst, illegal_pc;
	csr_addr_t csr_ra;
	u64 csr_rd;
	logic interrupt_trigger;

	// initial begin
	// 	commit_pc_save = '0;
	// end
	
	pcselect pcselect(
		.clk,
		.reset,
		.stalling(stall_hazard || stall_dbus || stall_ibus || stall_alu),
		.stall_hazard,
		.branch_taken,
		.branch_target,
		.pc_plus4(pc + 4),
		.pc,
		.data_ok(iresp.data_ok),
		.illegal_pc
	);
	always_comb begin
		branch_target = _branch_target;
		if(dataW_nxt.ctl.op == OP_MRET) branch_target = mepc;
		else if(dataW_nxt.priv_inst.exception_trigger || interrupt_trigger) branch_target = mtvec;
		else if(dataW_nxt.ctl.is_csr) branch_target = dataW_nxt.pc + 4;
		
	end
	assign branch_taken = _branch_taken || dataW_nxt.priv_inst.exception_trigger || interrupt_trigger
	|| dataW_nxt.ctl.op == OP_MRET || dataW_nxt.ctl.is_csr;
	assign flush_hazard = stall_hazard;
	hazard hazard(
		.branch_taken,
		.jump,
		.dataD,
		.dataE,
		.dataM,
		.dataW,
		.dataD_forward,
		.stall_hazard,
		.ra1,
		.ra2,
		.rd1,
		.rd2,
		.csr_ra,
		.csr_rd
	);


	// assign dataD = dataD_forward;
	


	
	
	assign ireq.addr = pc;
	assign ireq.valid = |pc && ~illegal_pc;
	// && ~dataF.ctl.exception_trigger
	// && ~dataD_forward.ctl.exception_trigger && ~dataE.exception_trigger && 
	// ~dataM.ctl.exception_trigger && ~dataW.ctl.exception_trigger;
	assign raw_instr = iresp.data;
	
	// u64 cnt;
	// always_ff@(posedge clk) begin
	// 	if(reset) cnt <= '0;
	// 	else if(dataW_nxt.pc == 64'h80008048) begin
	// 		cnt <= 10;
	// 	end else cnt <= cnt - 1;
	// end
	// always_comb begin
	// 	// if(pc >= 64'h8000_0000) begin
	// 	// $display("pc");
	// 	// $display(pc);
	// 	// $display(dataW_nxt.pc);
	// 	// if($signed(cnt) > 0) begin
	// 	// 	$display(commit_regs_nxt.mepc);
			
	// 	// end
	// 	if(dataW_nxt.pc == 64'h80008048) $display(commit_regs_nxt.mepc);
	// 	if(dataW_nxt.pc == 64'h80008074 || pc == 64'h80008068) begin
	// 		$display(dataW_nxt.pc);
	// 		$display(commit_regs_nxt.mepc);
	// 		$display(commit_regs_nxt.mstatus.mie);

	// 	end
	// 	// $display("ireq.valid:");
	// 	// $display(ireq.valid);
	// 	// $display("except_trigg:");
	// 	// $display(dataE.exception_trigger);
	// 	// $display(dataW_nxt.priv_inst.exception_trigger);
	// 	// end
	
	// end
	// always_ff@(posedge clk) begin
	// 	if(reset) cnt <= '0;
	// 	else if(dataW_nxt.pc == 64'h800060a8) begin
	// 		cnt <= cnt + 1;
	// 		$display(cnt);
	// 	end
	// end

	fetch fetch(
		.clk,
		.reset,
		.flush(jump || flush_ibus || dataE.priv_inst.exception_trigger || dataW_nxt.priv_inst.exception_trigger || interrupt_trigger
		|| dataD_forward.ctl.is_csr || dataE.ctl.is_csr || dataM.ctl.is_csr),
		.stalling(stall_hazard || stall_dbus || stall_alu),
		.stall_hazard,
		.raw_instr,
		.pc,
		.dataF,
		.branch_taken,
		.data_ok(iresp.data_ok)
		
	);
	// assign dataF.raw_instr = raw_instr;
	// assign dataF.pc = pc;
	

	
	
	decode decode(
		.clk,
		.reset,
		.stalling(stall_hazard || stall_dbus || stall_alu),
		.flush(jump || dataE.priv_inst.exception_trigger || dataW_nxt.priv_inst.exception_trigger || interrupt_trigger
		|| dataD_forward.ctl.is_csr || dataE.ctl.is_csr || dataM.ctl.is_csr),
		.dataF,
		.dataD
		// .rd1,
		// .rd2,
		// .ra1,
		// .ra2,
		// .imm
	);

	// u64 result;
	// assign result = {{52{raw_instr[31]}}, raw_instr[31:20]};
	// assign flush_alu = stall_alu;
	execute execute(
		.clk,
		.reset,
		.flush(flush_hazard || dataE.priv_inst.exception_trigger || dataW_nxt.priv_inst.exception_trigger || interrupt_trigger),
		.stalling(stall_dbus || stall_alu ),
		.dataD(dataD_forward),
		.dataE,
		._branch_taken,
		._branch_target,
		.stall_alu
	);

	
	// u1 stall_ibus_save;
	// always_ff @(posedge clk) begin
	// 	if(reset) stall_ibus_save <= '0;
	// 	else if(stall_hazard && branch_taken) stall_ibus_save <= stall_ibus;
	// 	else if(stall_ibus_save) stall_ibus_save <= '0;
	// end
	//dbus handshake
	
	assign stall_dbus = dreq.valid && ~dresp.data_ok;
	assign flush_dbus = dreq.valid && ~dresp.data_ok;
	//ibus handshake
	
	assign stall_ibus = ireq.valid && ~iresp.data_ok;
	// always_comb begin
		
	// end
	assign flush_ibus = ireq.valid && ~iresp.data_ok;
	memory memory(
		.clk,
		.reset,
		.stalling(stall_alu || stall_dbus),
		.flush(dataW_nxt.priv_inst.exception_trigger || interrupt_trigger),
		.dataE,
		.dataM,
		.md_beforecheck(dresp.data),
		.ma(dreq.addr),
		.wd_aftercheck(dreq.data),
		.strobe_aftercheck(dreq.strobe),
		.valid(dreq.valid),
		.dataW_nxt_is_csr(dataW_nxt.ctl.is_csr)
	);

	
	writeback writeback(
		.clk,
		.reset,
		.stalling(stall_alu || stall_dbus),
		.flush(1'b0),
		.dataM,
		.dataW,
		.dataW_nxt
	);




	regfile regfile(
		.clk, .reset,
		.ra1,
		.ra2,
		.rd1,
		.rd2,
		.wvalid(dataW_nxt.ctl.regwrite && ~dataW_nxt.priv_inst.exception_trigger),
		.wa(dataW_nxt.wa),
		.wd(dataW_nxt.wd)
	);
	csr_regs_t commit_regs_nxt;
	u2 commit_mode_nxt;
	csr csr(
		.clk, .reset,
		.valid(dataW_nxt.ctl.csrwrite),
		.is_mret(dataW_nxt.ctl.op == OP_MRET),
		.ra(csr_ra),
		.wa(dataW_nxt.csr_wa),
		.wd(dataW_nxt.csr_wd),
		.pc(dataW_nxt.pc),
		.pcs_pc(pc),
		.dataF_pc(dataF.pc),
		.dataD_pc(dataD.pc),
		.dataE_pc(dataE.pc),
		.rd(csr_rd),
		.mepc,
		.mtvec,
		.priv_inst(dataW_nxt.priv_inst),
		.trint,
		.swint,
		.exint,
		.interrupt_trigger,
		.commit_regs_nxt,
		.commit_mode_nxt
	);

	
	// always_ff @(posedge clk) begin
	// 	if(reset) commit_pc_save <= '0;
	// 	else 
	// 		commit_pc_save <= dataM.pc;
	// 	// else begin

	// 	// end
	// end	
	// always_comb begin
	// 	// commit_valid = dataM.ctl.op != NOP && ~stall_dbus && ~stall_alu;
		
	// 	if(dataM.pc == commit_pc_save || dataM.pc == '0) begin
	// 		commit_valid = 1'b0;
	// 	end else  commit_valid = 1'b1;
	// end	


`ifdef VERILATOR
	DifftestInstrCommit DifftestInstrCommit(
		.clock              (clk),
		.coreid             (0),
		.index              (0),
		.valid              (dataM.pc != 0 && ~stall_dbus && ~stall_alu),
		.pc                 (dataM.pc),
		.instr              (0),
		.skip               ((dataM.ctl.mem_write || dataM.ctl.memtoreg) && dataM.result[31] == 1'b0),
		.isRVC              (0),
		.scFailed           (0),
		.wen                (dataW_nxt.ctl.regwrite),
		.wdest              ({3'b0, dataW_nxt.wa}),
		.wdata              (dataW_nxt.wd)
	);
	      
	DifftestArchIntRegState DifftestArchIntRegState (
		.clock              (clk),
		.coreid             (0),
		.gpr_0              (regfile.regs_nxt[0]),
		.gpr_1              (regfile.regs_nxt[1]),
		.gpr_2              (regfile.regs_nxt[2]),
		.gpr_3              (regfile.regs_nxt[3]),
		.gpr_4              (regfile.regs_nxt[4]),
		.gpr_5              (regfile.regs_nxt[5]),
		.gpr_6              (regfile.regs_nxt[6]),
		.gpr_7              (regfile.regs_nxt[7]),
		.gpr_8              (regfile.regs_nxt[8]),
		.gpr_9              (regfile.regs_nxt[9]),
		.gpr_10             (regfile.regs_nxt[10]),
		.gpr_11             (regfile.regs_nxt[11]),
		.gpr_12             (regfile.regs_nxt[12]),
		.gpr_13             (regfile.regs_nxt[13]),
		.gpr_14             (regfile.regs_nxt[14]),
		.gpr_15             (regfile.regs_nxt[15]),
		.gpr_16             (regfile.regs_nxt[16]),
		.gpr_17             (regfile.regs_nxt[17]),
		.gpr_18             (regfile.regs_nxt[18]),
		.gpr_19             (regfile.regs_nxt[19]),
		.gpr_20             (regfile.regs_nxt[20]),
		.gpr_21             (regfile.regs_nxt[21]),
		.gpr_22             (regfile.regs_nxt[22]),
		.gpr_23             (regfile.regs_nxt[23]),
		.gpr_24             (regfile.regs_nxt[24]),
		.gpr_25             (regfile.regs_nxt[25]),
		.gpr_26             (regfile.regs_nxt[26]),
		.gpr_27             (regfile.regs_nxt[27]),
		.gpr_28             (regfile.regs_nxt[28]),
		.gpr_29             (regfile.regs_nxt[29]),
		.gpr_30             (regfile.regs_nxt[30]),
		.gpr_31             (regfile.regs_nxt[31])
	);
	      
	DifftestTrapEvent DifftestTrapEvent(
		.clock              (clk),
		.coreid             (0),
		.valid              (0),
		.code               (0),
		.pc                 (0),
		.cycleCnt           (0),
		.instrCnt           (0)
	);
	      
	DifftestCSRState DifftestCSRState(
		.clock              (clk),
		.coreid             (0),
		.priviledgeMode     (commit_mode_nxt),
		.mstatus            (commit_regs_nxt.mstatus),
		.sstatus            (commit_regs_nxt.mstatus & 64'h800000030001e000),
		.mepc               (commit_regs_nxt.mepc),
		.sepc               (0),
		.mtval              (commit_regs_nxt.mtval),
		.stval              (0),
		.mtvec              (commit_regs_nxt.mtvec),
		.stvec              (0),
		.mcause             (commit_regs_nxt.mcause),
		.scause             (0),
		.satp               (0),
		.mip                (commit_regs_nxt.mip),
		.mie                (commit_regs_nxt.mie),
		.mscratch           (commit_regs_nxt.mscratch),
		.sscratch           (0),
		.mideleg            (0),
		.medeleg            (0)
	      );
	      
	DifftestArchFpRegState DifftestArchFpRegState(
		.clock              (clk),
		.coreid             (0),
		.fpr_0              (0),
		.fpr_1              (0),
		.fpr_2              (0),
		.fpr_3              (0),
		.fpr_4              (0),
		.fpr_5              (0),
		.fpr_6              (0),
		.fpr_7              (0),
		.fpr_8              (0),
		.fpr_9              (0),
		.fpr_10             (0),
		.fpr_11             (0),
		.fpr_12             (0),
		.fpr_13             (0),
		.fpr_14             (0),
		.fpr_15             (0),
		.fpr_16             (0),
		.fpr_17             (0),
		.fpr_18             (0),
		.fpr_19             (0),
		.fpr_20             (0),
		.fpr_21             (0),
		.fpr_22             (0),
		.fpr_23             (0),
		.fpr_24             (0),
		.fpr_25             (0),
		.fpr_26             (0),
		.fpr_27             (0),
		.fpr_28             (0),
		.fpr_29             (0),
		.fpr_30             (0),
		.fpr_31             (0)
	);
	
`endif
endmodule
`endif