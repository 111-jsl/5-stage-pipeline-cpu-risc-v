`ifndef __DECODER_SV
`define __DECODER_SV

`ifdef VERILATOR 
`include "include/common.sv"
`include "include/pipes.sv"
`else 

`endif


module decoder
    import common::*; 
    import pipes::*;
    import csr_pkg::*;
    (
    input u32 raw_instr,
    input priv_inst_t priv_inst_nxt,
    output priv_inst_t priv_inst,
    output control_t ctl
);
    f7_t f7;
    assign f7 = raw_instr[6:0];

    f3_t f3;
    assign f3 = raw_instr[14:12];

    f7_t ff7;
    assign ff7 = raw_instr[31:25];

    f6_t f6;
    assign f6 = raw_instr[31:26];



    always_comb begin
        ctl = '0;
        priv_inst = priv_inst_nxt;
        // $display("dataF_ctl:");
        // $display(dataF_ctl);
        if(raw_instr == 32'h5006b) begin
            
        end else begin
            unique case(f7) 
                F7_ALUI: begin
                    ctl.en_ra1 = 1'b1;
                    ctl.regwrite = 1'b1;
                    // ctl.msize = MSIZE8;
                    unique case(f3)
                        F3_ADDI: begin
                            ctl.op = OP_ADDI;
                            ctl.alufunc = ALU_ADD;
                        end
                        F3_XORI: begin
                            ctl.op = OP_XORI;
                            ctl.alufunc = ALU_XOR;
                        end
                        F3_ORI: begin
                            ctl.op = OP_ORI;
                            ctl.alufunc = ALU_OR;
                        end
                        F3_ANDI: begin
                            ctl.op = OP_ANDI;
                            ctl.alufunc = ALU_AND;
                        end
                        F3_SLTI: begin
                            ctl.op = OP_SLTI;
                            ctl.alufunc = ALU_SLT;
                        end
                        F3_SLTIU: begin
                            ctl.op = OP_SLTIU;
                            ctl.alufunc = ALU_SLTU;
                        end
                        F3_SLI: begin
                            unique case(f6)
                                F6_SLLI: begin
                                    ctl.op = OP_SLLI;
                                    ctl.alufunc = ALU_SLL;
                                end
                                // F6_SLAI: begin
                                //     ctl.op = OP_SLAI;
                                //     ctl.alufunc = ALU_SLA;
                                // end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                end
                            endcase
                        end
                        F3_SRI: begin
                            unique case(f6)
                                F6_SRLI: begin
                                    ctl.op = OP_SRLI;
                                    ctl.alufunc = ALU_SRL;
                                end
                                F6_SRAI: begin
                                    ctl.op = OP_SRAI;
                                    ctl.alufunc = ALU_SRA;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        default: begin
                            priv_inst.exception_trigger = 1'b1;
                            priv_inst.mcause = ILL_INST;
                            priv_inst.mtval = {{32{1'b0}}, raw_instr};
                            
                        end
                    endcase
                end
                F7_ALU: begin
                    ctl.en_ra1 = 1'b1;
                    ctl.en_ra2 = 1'b1;
                    ctl.regwrite = 1'b1;
                    ctl.alusrc = REG_SRC;
                    // ctl.msize = MSIZE8;
                    unique case(f3)
                        F3_ASM: begin
                            unique case(ff7)
                                FF7_ADD: begin
                                    ctl.op = OP_ADD;
                                    ctl.alufunc = ALU_ADD;
                                end
                                FF7_SUB: begin
                                    ctl.op = OP_SUB;
                                    ctl.alufunc = ALU_SUB;
                                end
                                FF7_MUL: begin
                                    ctl.op = OP_MUL;
                                    ctl.alufunc = ALU_MUL;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        F3_AND_REMU: begin
                            unique case(ff7)
                                FF7_AND: begin
                                    ctl.op = OP_AND;
                                    ctl.alufunc = ALU_AND;
                                end
                                FF7_REMU: begin
                                    ctl.op = OP_REMU;
                                    ctl.alufunc = ALU_REMU;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        F3_OR_REM: begin
                            unique case(ff7)
                                FF7_OR: begin
                                    ctl.op = OP_OR;
                                    ctl.alufunc = ALU_OR;
                                end
                                FF7_REM: begin
                                    ctl.op = OP_REM;
                                    ctl.alufunc = ALU_REM;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                            
                        end
                        F3_XOR_DIV: begin
                            unique case(ff7)
                                FF7_XOR: begin
                                    ctl.op = OP_XOR;
                                    ctl.alufunc = ALU_XOR;
                                end
                                FF7_DIV: begin
                                    ctl.op = OP_DIV;
                                    ctl.alufunc = ALU_DIV;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        F3_SL: begin
                            unique case(ff7)
                                FF7_SLL: begin
                                    ctl.op = OP_SLL;
                                    ctl.alufunc = ALU_SLL;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        F3_SR_DIVU: begin
                            unique case(ff7)
                                FF7_SRL: begin
                                    ctl.op = OP_SRL;
                                    ctl.alufunc = ALU_SRL;
                                end
                                FF7_SRA: begin
                                    ctl.op = OP_SRA;
                                    ctl.alufunc = ALU_SRA;
                                end
                                FF7_DIVU: begin
                                    ctl.op = OP_DIVU;
                                    ctl.alufunc = ALU_DIVU;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        F3_SLT: begin
                            unique case(ff7)
                                FF7_SLT: begin
                                    ctl.op = OP_SLT;
                                    ctl.alufunc = ALU_SLT;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        F3_SLTU: begin
                            unique case(ff7)
                                FF7_SLTU: begin
                                    ctl.op = OP_SLTU;
                                    ctl.alufunc = ALU_SLTU;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        default: begin
                            priv_inst.exception_trigger = 1'b1;
                            priv_inst.mcause = ILL_INST;
                            priv_inst.mtval = {{32{1'b0}}, raw_instr};
                            
                        end
                    endcase
                end
                F7_ALUIW: begin
                    ctl.en_ra1 = 1'b1;
                    ctl.regwrite = 1'b1;
                    ctl.result_sext = 1'b1;
                
                    unique case(f3)
                        F3_ADDIW: begin
                            ctl.op = OP_ADDIW;
                            ctl.alufunc = ALU_ADD;
                        end
                        F3_SLIW: begin
                            unique case(f6)
                                F6_SLLIW: begin
                                    ctl.op = OP_SLLIW;
                                    ctl.alufunc = ALU_SLLW;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        F3_SRIW: begin
                            unique case(f6)
                                F6_SRLIW: begin
                                    ctl.op = OP_SRLIW;
                                    ctl.alufunc = ALU_SRLW;
                                end
                                F6_SRAIW: begin
                                    ctl.op = OP_SRAIW;
                                    ctl.alufunc = ALU_SRAW;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        default: begin
                            priv_inst.exception_trigger = 1'b1;
                            priv_inst.mcause = ILL_INST;
                            priv_inst.mtval = {{32{1'b0}}, raw_instr};
                            
                        end
                    endcase
                end
                F7_ALUW: begin
                    ctl.en_ra1 = 1'b1;
                    ctl.en_ra2 = 1'b1;
                    ctl.regwrite = 1'b1;
                    ctl.alusrc = REG_SRC;
                    ctl.result_sext = 1'b1;
                    unique case(f3)
                        F3_ASMW: begin
                            unique case(ff7)
                                FF7_ADDW: begin
                                    ctl.op = OP_ADDW;
                                    ctl.alufunc = ALU_ADD;
                                end
                                FF7_SUBW: begin
                                    ctl.op = OP_SUBW;
                                    ctl.alufunc = ALU_SUB;
                                end
                                FF7_MULW: begin
                                    ctl.op = OP_MULW;
                                    ctl.alufunc = ALU_MUL;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        F3_SLW: begin
                            unique case(ff7)
                                FF7_SLLW: begin
                                    ctl.op = OP_SLLW;
                                    ctl.alufunc = ALU_SLLW;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        F3_DIVW: begin
                            unique case(ff7)
                                FF7_DIVW: begin
                                    ctl.op = OP_DIVW;
                                    ctl.alufunc = ALU_DIV;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        F3_SRW_DIVUW: begin
                            unique case(ff7)
                                FF7_SRLW: begin
                                    ctl.op = OP_SRLW;
                                    ctl.alufunc = ALU_SRLW;
                                end
                                FF7_SRAW: begin
                                    ctl.op = OP_SRAW;
                                    ctl.alufunc = ALU_SRAW;
                                end
                                FF7_DIVUW: begin
                                    ctl.op = OP_DIVUW;
                                    ctl.alufunc = ALU_DIVU;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        F3_REMW: begin
                            unique case(ff7)
                                FF7_REMW: begin
                                    ctl.op = OP_REMW;
                                    ctl.alufunc = ALU_REM;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        F3_REMUW: begin
                            unique case(ff7)
                                FF7_REMUW: begin
                                    ctl.op = OP_REMUW;
                                    ctl.alufunc = ALU_REMU;
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        default: begin
                            
                            priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                        end
                    endcase
                end
                F7_LUI: begin
                    ctl.op = OP_LUI;
                    ctl.regwrite = 1'b1;
                    ctl.alufunc = ALU_ADD;
                    // ctl.msize = MSIZE8;
                end
                F7_JAL: begin
                    ctl.op = OP_JAL;
                    ctl.branch = 1'b1;
                    ctl.regwrite = 1'b1;
                    ctl.alufunc = ALU_ADD;
                end
                F7_JALR: begin
                    unique case(f3) 
                        F3_JALR: begin
                            ctl.op = OP_JALR;
                            ctl.branch = 1'b1;
                            ctl.regwrite = 1'b1;
                            ctl.alufunc = ALU_ADD;
                            ctl.en_ra1 = 1'b1;
                        end
                        default: begin
                            priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                        end
                    endcase
                end
                F7_BR: begin
                    ctl.alusrc = REG_SRC;
                    ctl.en_ra1 = 1'b1;
                    ctl.en_ra2 = 1'b1;
                    unique case(f3) 
                        F3_BEQ: begin
                            ctl.op = OP_BEQ;
                        end
                        F3_BNE: begin
                            ctl.op = OP_BNE;
                        end
                        F3_BLT: begin
                            ctl.op = OP_BLT;
                        end
                        F3_BGE: begin
                            ctl.op = OP_BGE;
                        end
                        F3_BLTU: begin
                            ctl.op = OP_BLTU;
                        end
                        F3_BGEU: begin
                            ctl.op = OP_BGEU;
                        end
                        default: begin
                            priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                            
                        end
                    endcase
                end
                F7_LD: begin
                    ctl.memtoreg = 1'b1;
                    ctl.regwrite = 1'b1;
                    ctl.alufunc = ALU_ADD;
                    ctl.en_ra1 = 1'b1;
                    unique case(f3)
                        F3_LD: begin
                            ctl.op = OP_LD;
                        end
                        F3_LB: begin
                            ctl.op = OP_LB;
                        end
                        F3_LH: begin
                            ctl.op = OP_LH;
                        end
                        F3_LW: begin
                            ctl.op = OP_LW;
                        end
                        F3_LBU: begin
                            ctl.op = OP_LBU;
                        end
                        F3_LHU: begin
                            ctl.op = OP_LHU;
                        end
                        F3_LWU: begin
                            ctl.op = OP_LWU;
                        end
                        default: begin
                            priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                            
                        end
                    endcase
                end
                F7_SD: begin
                    ctl.mem_write = 1'b1;
                    ctl.alufunc = ALU_ADD;
                    ctl.en_ra1 = 1'b1;
                    ctl.en_ra2 = 1'b1;
                    unique case(f3) 
                        F3_SD: begin
                            ctl.op = OP_SD;
                        end
                        F3_SB: begin
                            ctl.op = OP_SB;
                        end
                        F3_SH: begin
                            ctl.op = OP_SH;
                        end
                        F3_SW: begin
                            ctl.op = OP_SW;
                        end
                        default: begin
                            priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                            
                        end
                    endcase
                end
                F7_AUIPC: begin
                    ctl.op = OP_AUIPC;
                    ctl.regwrite = 1'b1;
                    ctl.alufunc = ALU_ADD;
                end
                F7_CSR: begin
                    ctl.is_csr = 1'b1;
                    unique case(f3)
                        F3_RC: begin
                            ctl.op = OP_CSRRC;
                            ctl.alufunc = ALU_AND;
                            ctl.en_ra1 = 1'b1;
                            ctl.alusrc = CSR_SRC;
                            ctl.regwrite = 1'b1;
                            ctl.csrwrite = 1'b1;
                            // ctl.is_csr = 1'b1;
                        end
                        F3_RCI: begin
                            ctl.op = OP_CSRRCI;
                            ctl.alufunc = ALU_AND;
                            ctl.regwrite = 1'b1;
                            ctl.csrwrite = 1'b1;
                            // ctl.is_csr = 1'b1;
                        end
                        F3_RS: begin
                            ctl.op = OP_CSRRS;
                            ctl.alufunc = ALU_OR;
                            ctl.en_ra1 = 1'b1;
                            ctl.alusrc = CSR_SRC;
                            ctl.regwrite = 1'b1;
                            ctl.csrwrite = 1'b1;
                            // ctl.is_csr = 1'b1;
                        end
                        F3_RSI: begin
                            ctl.op = OP_CSRRSI;
                            ctl.alufunc = ALU_OR;
                            ctl.regwrite = 1'b1;
                            ctl.csrwrite = 1'b1;
                            // ctl.is_csr = 1'b1;
                        end
                        F3_RW: begin
                            ctl.op =OP_CSRRW;
                            ctl.alufunc = ALU_OR;
                            ctl.en_ra1 = 1'b1;
                            ctl.alusrc = CSR_SRC;
                            ctl.regwrite = 1'b1;
                            ctl.csrwrite = 1'b1;
                            // ctl.is_csr = 1'b1;
                        end
                        F3_RWI: begin
                            ctl.op = OP_CSRRWI;
                            ctl.alufunc = ALU_OR;
                            ctl.regwrite = 1'b1;
                            ctl.csrwrite = 1'b1;
                            // ctl.is_csr = 1'b1;

                        end
                        F3_SPEC: begin
                            unique case(ff7)
                                FF7_MRET: begin
                                    ctl.op = OP_MRET;
                                end
                                FF7_ECALL: begin
                                    ctl.op = OP_ECALL;
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = U_ECALL;
                                    
                                    
                                end
                                default: begin
                                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                                    
                                end
                            endcase
                        end
                        default: begin
                            priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                            
                        end
                    endcase
                end
                F7_NOP: begin

                end
                default: begin
                    priv_inst.exception_trigger = 1'b1;
                                    priv_inst.mcause = ILL_INST;
                                    priv_inst.mtval = {{32{1'b0}}, raw_instr};
                    
                end
            endcase
            end
    end

endmodule
`endif