`ifndef __SEXT_SV
`define __SEXT_SV

`ifdef VERILATOR 

`else 

`endif

module sext 
	import common::*;
	import pipes::*; 
	(
    input u32 raw_instr,
    input op_t op,
    output u64 imm
	);
	//alui
    u64 imm_addi;
	assign imm_addi = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	u64 imm_xori;
	assign imm_xori = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	u64 imm_ori;
	assign imm_ori = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	u64 imm_andi;
	assign imm_andi = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	u64 imm_slti;
	assign imm_slti = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	u64 imm_sltiu;
	assign imm_sltiu = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	u64 imm_slli;
	assign imm_slli = {
		{58{raw_instr[25]}},
		raw_instr[25:20]
	};

	u64 imm_srli;
	assign imm_srli = {
		{58{raw_instr[25]}},
		raw_instr[25:20]
	};

	u64 imm_srai;
	assign imm_srai = {
		{58{raw_instr[25]}},
		raw_instr[25:20]
	};

	// u64 imm_alui;
	// assign imm_alui = {
	// 	52{raw_instr[31]},
	// 	raw_instr[31:20]
	// };

	//aluiw
	u64 imm_addiw;
	assign imm_addiw = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	u64 imm_slliw;
	assign imm_slliw = {
		{58{raw_instr[25]}},
		raw_instr[25:20]
	};

	u64 imm_srliw;
	assign imm_srliw = {
		{58{raw_instr[25]}},
		raw_instr[25:20]
	};

	u64 imm_sraiw;
	assign imm_sraiw = {
		{58{raw_instr[25]}},
		raw_instr[25:20]
	};

	

	//other
	u64 imm_lui;
	assign imm_lui = {
		{32{raw_instr[31]}},
		raw_instr[31:12],
		{12{1'b0}}
	};

	u64 imm_jal;
	assign imm_jal = {
		{43{raw_instr[31]}},
		raw_instr[31],
		raw_instr[19:12],
		raw_instr[20],
		raw_instr[30:21],
		{1'b0}
	};

	//br
	u64 imm_beq;
	assign imm_beq = {
		{51{raw_instr[31]}},
		raw_instr[31],
		raw_instr[7],
		raw_instr[30:25],
		raw_instr[11:8],
		{1'b0}
	};

	u64 imm_bne;
	assign imm_bne = {
		{51{raw_instr[31]}},
		raw_instr[31],
		raw_instr[7],
		raw_instr[30:25],
		raw_instr[11:8],
		{1'b0}
	};

	u64 imm_blt;
	assign imm_blt = {
		{51{raw_instr[31]}},
		raw_instr[31],
		raw_instr[7],
		raw_instr[30:25],
		raw_instr[11:8],
		{1'b0}
	};

	u64 imm_bge;
	assign imm_bge = {
		{51{raw_instr[31]}},
		raw_instr[31],
		raw_instr[7],
		raw_instr[30:25],
		raw_instr[11:8],
		{1'b0}
	};

	u64 imm_bltu;
	assign imm_bltu = {
		{51{raw_instr[31]}},
		raw_instr[31],
		raw_instr[7],
		raw_instr[30:25],
		raw_instr[11:8],
		{1'b0}
	};

	u64 imm_bgeu;
	assign imm_bgeu = {
		{51{raw_instr[31]}},
		raw_instr[31],
		raw_instr[7],
		raw_instr[30:25],
		raw_instr[11:8],
		{1'b0}
	};
	
	//load
	u64 imm_ld;
	assign imm_ld = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	u64 imm_lb;
	assign imm_lb = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	u64 imm_lh;
	assign imm_lh = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	u64 imm_lw;
	assign imm_lw = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	u64 imm_lbu;
	assign imm_lbu = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	u64 imm_lhu;
	assign imm_lhu = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	u64 imm_lwu;
	assign imm_lwu = {
		{52{raw_instr[31]}},
		raw_instr[31:20]
	};

	//save
	u64 imm_sd;
	assign imm_sd = {
		{52{raw_instr[31]}},
		raw_instr[31:25],
		raw_instr[11:7]
	};

	u64 imm_sb;
	assign imm_sb = {
		{52{raw_instr[31]}},
		raw_instr[31:25],
		raw_instr[11:7]
	};
	
	u64 imm_sh;
	assign imm_sh = {
		{52{raw_instr[31]}},
		raw_instr[31:25],
		raw_instr[11:7]
	};

	u64 imm_sw;
	assign imm_sw = {
		{52{raw_instr[31]}},
		raw_instr[31:25],
		raw_instr[11:7]
	};

	//other

	u64 imm_auipc;
	assign imm_auipc = {
		{32{raw_instr[31]}},
		raw_instr[31:12],
		{12{1'b0}}
	};

	u64 imm_jalr;
	assign imm_jalr = {
		{52{raw_instr[31]}},
		{raw_instr[31:20]}
	};

	u64 zimm;
	assign zimm = {
		{59{1'b0}},
		raw_instr[19:15]
	};

	always_comb begin
		unique case(op)
			//alui
			OP_ADDI: begin
				imm = imm_addi;
			end
			OP_XORI: begin
				imm = imm_xori;
			end
			OP_ORI: begin
				imm = imm_ori;
			end
			OP_ANDI: begin
				imm = imm_andi;
			end
			OP_SLTI: begin
				imm = imm_slti;
			end
			OP_SLTIU: begin
				imm = imm_sltiu;
			end
			OP_SLLI: begin
				imm = imm_slli;
			end
			OP_SRLI: begin
				imm = imm_srli;
			end
			OP_SRAI: begin
				imm = imm_srai;
			end
			//aluiw
			OP_ADDIW: begin
				imm = imm_addiw;
			end
			OP_SLLIW: begin
				imm = imm_slliw;
			end
			OP_SRLIW: begin
				imm = imm_srliw;
			end
			OP_SRAIW: begin
				imm = imm_sraiw;
			end
			//other
			OP_LUI: begin
				imm = imm_lui;
			end
			OP_JAL: begin
				imm = imm_jal;
			end
			//branch
			OP_BEQ: begin
				imm = imm_beq;
			end
			OP_BNE: begin
				imm = imm_bne;
			end
			OP_BLT: begin
				imm = imm_blt;
			end
			OP_BGE: begin
				imm = imm_bge;
			end
			OP_BLTU: begin
				imm = imm_bltu;
			end
			OP_BGEU: begin
				imm = imm_bgeu;
			end
			//load
			OP_LD: begin
				imm = imm_ld;
			end
			OP_LB: begin
				imm = imm_lb;
			end
			OP_LH: begin
				imm = imm_lh;
			end
			OP_LW: begin
				imm = imm_lw;
			end
			OP_LBU: begin
				imm = imm_lbu;
			end
			OP_LHU: begin
				imm = imm_lhu;
			end
			OP_LWU: begin
				imm = imm_lwu;
			end
			//save
			OP_SD: begin
				imm = imm_sd;
			end
			OP_SB: begin
				imm = imm_sb;
			end
			OP_SH: begin
				imm = imm_sh;
			end
			OP_SW: begin
				imm = imm_sw;
			end
			//other
			OP_AUIPC: begin
				imm = imm_auipc;
			end
			OP_JALR: begin
				imm = imm_jalr;
			end
			OP_CSRRC: imm = zimm;
			OP_CSRRCI: imm = zimm;
			OP_CSRRS: imm = zimm;
			OP_CSRRSI: imm = zimm;
			OP_CSRRW: imm = zimm;
			OP_CSRRWI: imm = zimm;
			default: begin
                imm = 'x;
			end
		endcase
	end

endmodule
`endif