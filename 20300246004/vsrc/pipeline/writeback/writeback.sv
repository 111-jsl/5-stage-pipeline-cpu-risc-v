`ifndef __WRITEBACK_SV
`define __WRITEBACK_SV
`ifdef VERILATOR
`include "include/common.sv"
`include "include/pipes.sv"
`else

`endif


module writeback
    import common::*;
    import pipes::*;
    (
        input logic clk, reset,
        input logic stalling, flush,
        input memory_data_t dataM,
        output writeback_data_t dataW, dataW_nxt
    );
    
    initial begin
        dataW = '0;
        dataW_nxt = '0;
    end

    always_comb begin
        if(dataM.ctl.regwrite) begin
            if(dataM.ctl.memtoreg) begin
                dataW_nxt.wd = dataM.md;
            end else if(dataM.ctl.csrwrite) begin
                dataW_nxt.wd = dataM.csr_rd;
            end else begin
                dataW_nxt.wd = dataM.result;
            end
        end else begin
            dataW_nxt.wd = '0;
        end
    end

    assign dataW_nxt.csr_wd = dataM.result;
    assign dataW_nxt.csr_wa = dataM.csr_wa;
    assign dataW_nxt.wa = dataM.dst;
    assign dataW_nxt.ctl = dataM.ctl;
    
    assign dataW_nxt.pc = dataM.pc;
    assign dataW_nxt.priv_inst = dataM.priv_inst;

    always_ff @(posedge clk) begin
        if(reset) dataW <= '0;
        else if(stalling) begin

        end else if(flush) begin
            dataW <= '0;
        end
        else dataW <= dataW_nxt;
    end

    // always_comb begin
    //     $display("dataW:");
    //     $display(dataW_nxt.pc);
    //     $display(dataW_nxt.mcause);
    //     $display(dataW_nxt.exception_trigger);
    // end
endmodule
`endif    