`ifndef __WRITEDATA_SV
`define __WRITEDATA_SV
`ifdef VERILATOR
`include "include/common.sv"
`include "include/pipes.sv"
`else

`endif

module writedata
    import common::*;
    import pipes::*;
    (
        input u64 wd_beforecheck,
        output u64 wd_aftercheck,
        input u3 addr,
        input msize_t msize,
        output strobe_t strobe_beforecheck
    );

    always_comb begin
        strobe_beforecheck = '0;
        wd_aftercheck = 'x;
        unique case(msize)
            MSIZE1: begin
                unique case(addr)
                    3'b000: begin
                        strobe_beforecheck = 8'h01;
                        wd_aftercheck[7-:8] = wd_beforecheck[7:0];
                    end
                    3'b001: begin
                        strobe_beforecheck = 8'h02;
                        wd_aftercheck[15-:8] = wd_beforecheck[7:0];
                    end
                    3'b010: begin
                        strobe_beforecheck = 8'h04;
                        wd_aftercheck[23-:8] = wd_beforecheck[7:0];
                    end
                    3'b011: begin
                        strobe_beforecheck = 8'h08;
                        wd_aftercheck[31-:8] = wd_beforecheck[7:0];
                    end
                    3'b100: begin
                        strobe_beforecheck = 8'h10;
                        wd_aftercheck[39-:8] = wd_beforecheck[7:0];
                    end
                    3'b101: begin
                        strobe_beforecheck = 8'h20;
                        wd_aftercheck[47-:8] = wd_beforecheck[7:0];
                    end
                    3'b110: begin
                        strobe_beforecheck = 8'h40;
                        wd_aftercheck[55-:8] = wd_beforecheck[7:0];
                    end
                    3'b111: begin
                        strobe_beforecheck = 8'h80;
                        wd_aftercheck[63-:8] = wd_beforecheck[7:0];
                    end
                    default: begin
                        
                    end
                endcase
            end
            MSIZE2: begin
                unique case(addr)
                    3'b000: begin
                        strobe_beforecheck = 8'h03;
                        wd_aftercheck[15-:16] = wd_beforecheck[15:0];
                    end
                    3'b010: begin
                        strobe_beforecheck = 8'h0c;
                        wd_aftercheck[31-:16] = wd_beforecheck[15:0];
                    end
                    3'b100: begin
                        strobe_beforecheck = 8'h30;
                        wd_aftercheck[47-:16] = wd_beforecheck[15:0];
                    end
                    3'b110: begin
                        strobe_beforecheck = 8'hc0;
                        wd_aftercheck[63-:16] = wd_beforecheck[15:0];
                    end
                    default: begin
                        
                    end
                endcase
            end
            MSIZE4: begin
                unique case(addr)
                    3'b000: begin
                        strobe_beforecheck = 8'h0f;
                        wd_aftercheck[31-:32] = wd_beforecheck[31:0];
                    end
                    3'b100: begin
                        strobe_beforecheck = 8'hf0;
                        wd_aftercheck[63-:32] = wd_beforecheck[31:0];
                    end
                    default: begin
                        
                    end
                endcase
            end
            MSIZE8: begin
                unique case(addr)
                    3'b000: begin
                        strobe_beforecheck = '1;
                        wd_aftercheck = wd_beforecheck;
                    end
                    default: begin
                        
                    end
                endcase
            end
            default: begin
                
            end
        endcase
    end





endmodule
`endif