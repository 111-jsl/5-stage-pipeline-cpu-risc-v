## Lab4 异常与中断

![csr_sketch](E:\fudan\sophomore\pocc\2022Spring\Arch-2022Spring-FDU\vsrc\pipeline\csr\csr_sketch.png)

- csr指令实现

> csr指令格式：`x[rs1] = csr_rd;`    	`csr_rd = csr_rd + rd1/zimm`（+泛指运算）

1. rd1为rd1和zimm；
2. rd2为csr_rd；
3. csr_wd为rd1和rd2在E阶段得到的result
4. csr_wa为csr_rd对应的csr_ra；
5. wd为csr_rd；
6. dst为rs1；

D阶段就能发现是不是csr指令，若是，则刷新后面的流水段，直到该指令完成





- 异常实现

异常最晚在M阶段发现，所以在M阶段：

1. dreq.valid = 0
2. 刷新流水线(保证该异常指令进入W阶段，后面的指令不会发起读请求)
3. 修改csr寄存器
4. 修改branch_taken = 1 & branch_target = mtvec



- 中断实现

中断是不定时出现的，有可能W阶段时指令是气泡，此时中断开启，将读到错误的mepc

解决方法是：

各阶段的pc都接到csr模块中，哪个不为0，mepc接哪一个。

```verilog
if(pc != 0) regs_nxt.mepc = pc;
else if(dataE_pc != 0) regs_nxt.mepc = dataE_pc;
else if(dataD_pc != 0) regs_nxt.mepc = dataD_pc;
else if(dataF_pc != 0) regs_nxt.mepc = dataF_pc;
else if(pcselect_pc != 0) regs_nxt.mepc = pcselect_pc;
else begin end
```



测试成功截图：

![image-20220609135744870](E:\fudan\sophomore\pocc\experiment\lab4\image-20220609135744870.png)