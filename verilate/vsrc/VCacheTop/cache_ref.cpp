#include "mycache.h"
#include "cache_ref.h"

CacheRefModel::CacheRefModel(MyCache *_top, size_t memory_size)
	: top(_top), scope(top->VCacheTop), mem(memory_size)
{
	mem.set_name("ref");
#ifdef REFERENCE_CACHE
	// REFERENCE_CACHE does nothing else
#else
	/**
	 * TODO (Lab3) setup reference model :)
	 */
	counter = 0;
	for(int i = 0; i < 8; i++){
		age[i][0] = 0;
		age[i][1] = 0;
	}
	for(int i = 0; i < 0x80; i++){
		buffer[i][0] = 0;
		buffer[i][1] = 0;
	}
	for(int i = 0; i < 8; i++){
		meta[i][0].valid = 0;
		meta[i][1].valid = 0;
		meta[i][0].tag = 0;
		meta[i][1].tag = 0;
		meta[i][0].dirty = 0;
		meta[i][1].dirty = 0;
	}
#endif
}

void CacheRefModel::reset()
{
	log_debug("ref: reset()\n");
	mem.reset();
#ifdef REFERENCE_CACHE
	// REFERENCE_CACHE does nothing else
#else
	/**
	 * TODO (Lab3) reset reference model :)
	 */
	// ByteSeq reset_data;
	// reset_data.resize(mem.size());
	// mem.map(0, reset_data);
	counter = 0;
	for(int i = 0; i < 8; i++){
		age[i][0] = 0;
		age[i][1] = 0;
	}
	for(int i = 0; i < 0x80; i++){
		buffer[i][0] = 0;
		buffer[i][1] = 0;
	}
	for(int i = 0; i < 8; i++){
		meta[i][0].valid = 0;
		meta[i][1].valid = 0;
		meta[i][0].tag = 0;
		meta[i][1].tag = 0;
		meta[i][0].dirty = 0;
		meta[i][1].dirty = 0;
	}
#endif
}

auto CacheRefModel::load(addr_t addr, AXISize size) -> word_t
{
	log_debug("ref: load(0x%lx, %d)\n", addr, 1 << size);
#ifdef REFERENCE_CACHE
	addr_t start = addr / 128 * 128;
	for (int i = 0; i < 16; i++) {
		buffer[i] = mem.load(start + 8 * i);
	}

	return buffer[addr % 128 / 8];
#else
	/**
	 * TODO (Lab3) implement load operation for reference model :)
	 */
	addr_t tag = (addr & 0x00000000fffffc00) >> 10;
	// log_debug("what is the tag? %016x\n", tag);
	addr_t index = ((addr >> 7) & 0x7);
	addr_t data_index = index << 4;
	addr_t start = addr / 128 * 128;
	addr_t store_start = 0;
	addr_t offset = (addr >> 3) % 0x10;
	int select = -1;
	if(meta[index][0].tag == tag && meta[index][0].valid){
		age[index][0] = counter++;
		counter_overflow_check();
		return buffer[data_index + offset][0];
	}
	else if(meta[index][1].tag == tag && meta[index][1].valid){
		age[index][1] = counter++;
		counter_overflow_check();
		return buffer[data_index + offset][1];
	}
	else{
		if(!meta[index][0].valid){
			select = 0;
		}
		else if(!meta[index][1].valid){
			select = 1;
		}
		else{
			select = age[index][0] > age[index][1];
			store_start = (addr & 0xffffffff00000000) + ((meta[index][select].tag & 0x3fffff) << 10) + (data_index << 3);
			if(meta[index][select].dirty){
				for(int i = 0; i < 16; i++){
					mem.store(store_start + (i << 3), buffer[data_index + i][select], 0xffffffffffffffff);
				}
			}
		}

		
		// const int total = RAM_SIZE >> 1;
		for (int i = 0; i < 16; i++) {
			asserts(select >= 0, "segmentation fault");
			buffer[data_index + i][select] = mem.load(start + 8 * i);
			
		}
		age[index][select] = counter++;
		counter_overflow_check();
		meta[index][select].tag = tag;
		// log_debug("meta tag: %016x\n", meta[index][select].tag);
		// for(int i = 0; i < 8; i++){
		// 	for(int j = 0; j < 2; j++){
		// 		log_debug("%016x ", meta[i][j].tag);
		// 	}
		// 	log_debug("\n");
		// }
		meta[index][select].valid = 1;
		meta[index][select].dirty = 0;
		return buffer[data_index + offset][select];
	}
#endif
}

void CacheRefModel::store(addr_t addr, AXISize size, word_t strobe, word_t data)
{

	log_debug("ref: store(0x%lx, %d, %x, \"%016x\")\n", addr, 1 << size, strobe, data);
#ifdef REFERENCE_CACHE
	addr_t start = addr / 128 * 128;
	for (int i = 0; i < 16; i++) {
		buffer[i] = mem.load(start + 8 * i);
	}

	auto mask1 = STROBE_TO_MASK[strobe & 0xf];
	auto mask2 = STROBE_TO_MASK[((strobe) >> 4) & 0xf];
	auto mask = (mask2 << 32) | mask1;
	auto &value = buffer[addr % 128 / 8];
	value = (data & mask) | (value & ~mask);
	mem.store(addr, data, mask);
	return;
#else
	/**
	 * TODO (Lab3) implement store operation for reference model :)
	 */
	addr_t tag = (addr & 0x00000000fffffc00) >> 10;
	// log_debug("what is the tag? %016x\n", tag);
	
	addr_t index = ((addr >> 7) & 0x7);
	addr_t data_index = index << 4;
	addr_t start = addr / 128 * 128;
	addr_t store_start = 0;
	addr_t offset = (addr >> 3) % 0x10;

	auto mask1 = STROBE_TO_MASK[strobe & 0xf];
	auto mask2 = STROBE_TO_MASK[((strobe) >> 4) & 0xf];
	auto mask = (mask2 << 32) | mask1;

	int select = -1;
	if(meta[index][0].tag == tag && meta[index][0].valid){
		age[index][0] = counter++;
		counter_overflow_check();
		meta[index][0].dirty = 1;
		// log_debug("store[0] offset: %d\n", offset);
		auto &value = buffer[data_index + offset][0];
		value = (data & mask) | (value & ~mask);
	}
	else if(meta[index][1].tag == tag && meta[index][1].valid){
		age[index][1] = counter++;
		counter_overflow_check();
		meta[index][1].dirty = 1;
		auto &value = buffer[data_index + offset][1];
		value = (data & mask) | (value & ~mask);
	}
	else{
		if(!meta[index][0].valid){
			select = 0;
		}
		else if(!meta[index][1].valid){
			select = 1;
		}
		else{
			select = age[index][0] > age[index][1];
			// for(int i = 0; i < 8; i++){
			// for(int j = 0; j < 2; j++){
			// 		log_debug("%016x ", meta[i][j].tag);
			// 	}
			// 	log_debug("\n");
			// }
			// log_debug("data: %016x\n", buffer[data_index + offset][select]);
			// log_debug("index: %016x\nselect: %016x\nwriteback tag: %016x\n", index, select, meta[index][select].tag);
			// log_debug("writeback index: %016x\n", data_index);
			store_start =  (addr & 0xffffffff00000000) + ((meta[index][select].tag & 0x3fffff) << 10) + (data_index << 3);
			// log_debug("store start: %lld", store_start);
			// log_debug("writeback: %016x", data_index);
			if(meta[index][select].dirty){
				for(int i = 0; i < 16; i++){
					mem.store(store_start + (i << 3), buffer[data_index + i][select], 0xffffffffffffffff);
				}
			}
		}

		
		// const int total = RAM_SIZE >> 1;
		for (int i = 0; i < 16; i++) {
			asserts(select >= 0, "segmentation fault");
			buffer[data_index + i][select] = mem.load(start + 8 * i);
			
		}
		age[index][select] = counter++;
		counter_overflow_check();
		meta[index][select].tag = tag;
		// log_debug("meta tag: %016x\n", meta[index][select].tag);
		// for(int i = 0; i < 8; i++){
		// 	for(int j = 0; j < 2; j++){
		// 		log_debug("%016x ", meta[i][j].tag);
		// 	}
		// 	log_debug("\n");
		// }
		meta[index][select].valid = 1;
		meta[index][select].dirty = 1;
		// log_debug("select what??? %d\n", select);
		auto &value = buffer[data_index + offset][select];
		value = (data & mask) | (value & ~mask);
	}
	
	// for (int i = 0; i < 16; i++) log_debug("buffer:%d => %d\n", i, buffer[i]);
	// mem.store(addr, data, mask);
	return;
#endif
}

void CacheRefModel::check_internal()
{
	log_debug("ref: check_internal()\n");
#ifdef REFERENCE_CACHE
	/**
	 * the following comes from StupidBuffer's reference model.
	 */
	for (int i = 0; i < 16; i++) {
		asserts(
			buffer[i] == scope->mem[i],
			"reference model's internal state is different from RTL model."
			" at mem[%x], expected = %016x, got = %016x",
			i, buffer[i], scope->mem[i]
		);
	}
#else
	/**
	 * TODO (Lab3) compare reference model's internal states to RTL model :)
	 *
	 * NOTE: you can use pointer top and scope to access internal signals
	 *       in your RTL model, e.g., top->clk, scope->mem.
	 */
	// auto mask1 = STROBE_TO_MASK[top->ref_strobe & 0xf];
	// auto mask2 = STROBE_TO_MASK[((top->ref_strobe) >> 4) & 0xf];
	// auto mask = (mask2 << 32) | mask1;
	// for (int i = 0; i < 16; i++) log_debug("check_internal::buffer:%d => %d\n", i, buffer[i]);
	for (int i = 0; i < 0x80; i++) {
		asserts(
			buffer[i][0] == scope->mem[i][0],
			
			"reference model's internal state is different from RTL model."
			" at mem[%x], expected = %016x, got = %016x",
			i, buffer[i][0], scope->mem[i][0]
		);
		asserts(
			buffer[i][1] == scope->mem[i][1],
			
			"reference model's internal state is different from RTL model."
			" at mem[%x], expected = %016x, got = %016x",
			i, buffer[i][1], scope->mem[i][1]
		);
	}
#endif
}

void CacheRefModel::check_memory()
{
	log_debug("ref: check_memory()\n");
#ifdef REFERENCE_CACHE
	/**
	 * the following comes from StupidBuffer's reference model.
	 */
	asserts(mem.dump(0, mem.size()) == top->dump(), "reference model's memory content is different from RTL model");
#else
	/**
	 * TODO (Lab3) compare reference model's memory to RTL model :)
	 *
	 * NOTE: you can use pointer top and scope to access internal signals
	 *       in your RTL model, e.g., top->clk, scope->mem.
	 *       you can use mem.dump() and MyCache::dump() to get the full contents
	 *       of both memories.
	 */
	// log_debug("mem.size(): %d\n", mem.size());
	// fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck
	// fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck
	// fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck
	// fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck
	// fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck
	// fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck fuckfuckfuckfuckfuckfuck
	// auto mem_dump = mem.dump(0, mem.size());
	// auto top_dump = top->dump();
	// asserts(mem_dump.size() == top_dump.size(), "memory size different\n");
	// for(int i = 0; i < top_dump.size(); i++){
	// 	asserts(mem_dump[i] == top_dump[i], "NO.%016x asserts fail: mem = %016x, top = %016x\n", i, mem_dump[i], top_dump[i]);
	// }
	asserts(mem.dump(0, mem.size()) == top->dump(), "reference model's memory content is different from RTL model");

#endif
}

void CacheRefModel::counter_overflow_check(){
	if(counter == 128){
		for(int i = 0; i < 8; i++){
			if(age[i][0] > age[i][1]){
				age[i][0] = 1;
				age[i][1] = 0;
			}
			else{
				age[i][0] = 0;
				age[i][1] = 1;
			}
		}
	}
}
